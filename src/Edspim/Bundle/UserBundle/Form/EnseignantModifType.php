<?php

namespace Edspim\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnseignantModifType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('email', 'text')
                ->add('institut', 'text', array(
                    'required' => false,
                ))
                ->add('labDpt', 'text', array(
                    'required' => false,
                ))
                ->add('fonction', 'text', array(
                    'required' => false,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\AppBundle\Entity\Enseignant'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'edspim_bundle_appbundle_enseignantmodif';
    }

}
