<?php

namespace Edspim\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnseignantType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('nom', 'text', array(
                    "attr" => array(
                        "class" => "form-control"
            )))
                ->add('prenom', 'text', array(
                    "attr" => array(
                        "class" => "form-control"
            )))
                ->add('institut', 'text', array(
                    'required' => false,
                    "attr" => array(
                        "class" => "form-control"
            )))
                ->add('labDpt', 'text', array(
                    'required' => false,
                    "attr" => array(
                        "class" => "form-control"
            )))
                ->add('fonction', 'text', array(
                    'required' => false,
                    "attr" => array(
                        "class" => "form-control"
            )))
                ->add('valider', 'submit', array(
                    "attr" => array(
                        "class" => "btn btn-success"
            )))
                ->add('supprimer', 'submit', array(
                    "attr" => array(
                        "class" => "btn btn-danger"
        )));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\AppBundle\Entity\Enseignant'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'edspim_bundle_appbundle_enseignant';
    }

}
