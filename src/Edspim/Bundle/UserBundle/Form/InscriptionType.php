<?php

namespace Edspim\Bundle\UserBundle\Form;

use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InscriptionType extends BaseType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        parent::buildForm($builder, $options);
        $builder->add('nom', 'text', array(
            'label' => 'utilisateur.nom'
        ))
                ->add('prenom', 'text', array(
                    'label' => 'utilisateur.prenom'
                ))
                ->add('statut', 'choice', array(
                    'choices' =>
                    array(
                        'utilisateur.statut.doctorant' => 'utilisateur.statut.doctorant',
                        'utilisateur.statut.enseignant' => 'utilisateur.statut.enseignant',
                        'utilisateur.statut.ed' => 'utilisateur.statut.ed',
                        'utilisateur.statut.externe' => 'utilisateur.statut.externe',
            )))
                ->add('inscription', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'edspim_bundle_userbundle_inscription';
    }

}
