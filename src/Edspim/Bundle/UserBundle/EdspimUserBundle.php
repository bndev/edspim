<?php

namespace Edspim\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EdspimUserBundle extends Bundle {

    public function getParent() {
        return 'FOSUserBundle';
    }

}
