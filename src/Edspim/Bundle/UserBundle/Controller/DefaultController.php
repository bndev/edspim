<?php

namespace Edspim\Bundle\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;

class DefaultController extends Controller
{

    /**
     * Vue et gestion du formulaire d'inscription au portail
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function inscriptionAction(Request $request) {

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(false);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);
        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                $userManager->updateUser($user);
                $this->sendMailInscription($user);
                if (null === $response = $event->getResponse()) {
                    $url = $this->container->get('router')->generate('edspim_index');
                    $response = new RedirectResponse($url);
                }
                return $response;
            }
        }

        return $this->render('EdspimUserBundle:Default:inscription.html.twig', array(
                    'form' => $form->createView(),
        ));
    }
    
    private function sendMailInscription($user) {
        $message = \Swift_Message::newInstance()
                        ->setSubject('Inscription ED SPIM')
                        ->setFrom('send@example.com')
                        ->setTo($user->getEmail())
                        ->setBody($this->renderView('EdspimUserBundle:Default:mail_inscription.html.twig', array(
                            'username' => $user->getUsername(),
                            'statut' => $user->getStatut())))
                ;
        $this->get('mailer')->send($message);
    }
}
