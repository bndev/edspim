<?php

// src/Edspim/Bundle/UserBundle/Entity/User.php

namespace Edspim\Bundle\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="Utilisateur")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User extends BaseUser {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @Assert\Email()
     */
    protected $email;

    /**
     *
     * @Assert\Length(
     *      min = "6"
     * )
     */
    protected $plainPassword;

    /**
     * @var string
     * 
     * @ORM\Column(name="nom", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    protected $nom;

    /**
     * @var string
     * 
     * @ORM\Column(name="prenom", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    protected $prenom;

    /**
     * @var string
     * 
     * @ORM\Column(name="statut", type="string")
     * 
     * @Assert\Choice(choices = 
     *          {
     *          "utilisateur.statut.doctorant", 
     *          "utilisateur.statut.enseignant", 
     *          "utilisateur.statut.ed", 
     *          "utilisateur.statut.externe"
     *          }, 
     *          message = "Choisissez un statut valide.")
     */
    protected $statut;

    /**
     * @var datetime
     * 
     * @ORM\Column(name="date_inscription", type="datetime")
     */
    protected $dateInscription;

    /**
     * @ORM\OneToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Doctorant", cascade={"persist"})
     */
    protected $doctorant;

    /**
     * @ORM\OneToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Enseignant", cascade={"persist"})
     */
    protected $enseignant;

    /**
     * @ORM\OneToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\ED", cascade={"persist"})
     */
    protected $ed;

    /**
     * Constructeur par défaut
     */
    public function __construct() {
        parent::__construct();
        $this->dateInscription = new \DateTime();
        $this->statut = "externe";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set dateInscription
     *
     * @param \DateTime $dateInscription
     * @return User
     */
    public function setDateInscription($dateInscription) {
        $this->dateInscription = $dateInscription;

        return $this;
    }

    /**
     * Get dateInscription
     *
     * @return \DateTime 
     */
    public function getDateInscription() {
        return $this->dateInscription;
    }

    /**
     * Set doctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return User
     */
    public function setDoctorant(\Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant = null) {
        $this->doctorant = $doctorant;

        return $this;
    }

    /**
     * Get doctorant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Doctorant 
     */
    public function getDoctorant() {
        return $this->doctorant;
    }

    /**
     * Set enseignant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Enseignant $enseignant
     * @return User
     */
    public function setEnseignant(\Edspim\Bundle\AppBundle\Entity\Enseignant $enseignant = null) {
        $this->enseignant = $enseignant;

        return $this;
    }

    /**
     * Get enseignant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Enseignant 
     */
    public function getEnseignant() {
        return $this->enseignant;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return User
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return User
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set statut
     *
     * @param string $statut
     * @return User
     */
    public function setStatut($statut) {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string 
     */
    public function getStatut() {
        return $this->statut;
    }


    /**
     * Set ed
     *
     * @param \Edspim\Bundle\AppBundle\Entity\ED $ed
     * @return User
     */
    public function setEd(\Edspim\Bundle\AppBundle\Entity\ED $ed = null)
    {
        $this->ed = $ed;

        return $this;
    }

    /**
     * Get ed
     *
     * @return \Edspim\Bundle\AppBundle\Entity\ED 
     */
    public function getEd()
    {
        return $this->ed;
    }
    
}
