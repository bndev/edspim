<?php

namespace Edspim\Bundle\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SeanceType extends AbstractType {

    private $salle;
    private $batiment;
    private $ville;
    private $adresse;

    public function __construct($salle, $batiment, $ville, $adresse) {
        $this->salle = $salle;
        $this->batiment = $batiment;
        $this->ville = $ville;
        $this->adresse = $adresse;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('typeSeance', 'choice', array(
                    'choices' => array(
                        'seance.typeSeance.cours' => 'seance.typeSeance.cours', 
                        'seance.typeSeance.td' => 'seance.typeSeance.td',
                        'seance.typeSeance.tp' => 'seance.typeSeance.tp'),
                    'attr' => array(
                        'class' => 'form-control'
            )))
                ->add('nbHeures', 'integer', array(
                    'attr' => array(
                        'class' => 'form-control'
            )))
                ->add('dateSeance', 'date')
                ->add('heureSeance', 'time')
                ->add('salle', 'text', array(
                    'attr' => array(
                        'class' => 'form-control',
                        'value' => $this->salle,
            )))
                ->add('batiment', 'text', array(
                    'attr' => array(
                        'class' => 'form-control',
                        'value' => $this->batiment,
            )))
                ->add('adresse', 'text', array(
                    'attr' => array(
                        'class' => 'form-control',
                        'value' => $this->adresse,
            )))
                ->add('lieu', 'text', array(
                    'attr' => array(
                        'class' => 'form-control',
                        'value' => $this->ville,
            )))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\AppBundle\Entity\Seance'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'edspim_bundle_appbundle_seance';
    }

}
