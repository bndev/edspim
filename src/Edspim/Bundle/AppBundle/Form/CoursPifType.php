<?php

namespace Edspim\Bundle\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CoursPifType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomCoursPif', 'text')
            ->add('anneePif', 'integer')
            ->add('typeCours', 'choice', array(
                    'label' => 'cours.typeCours.nom',
                    'choices' => array(
                        "cours.typeCours.formation" => "cours.typeCours.formation",
                        "cours.typeCours.boite" => "cours.typeCours.boite",
                        "cours.typeCours.coursED" => "cours.typeCours.coursED",
                        "cours.typeCours.tutoCongres" => "cours.typeCours.tutoCongres",
                        "cours.typeCours.ecoleEte" => "cours.typeCours.ecoleEte",
                        "cours.typeCours.autre" => "cours.typeCours.autre",
                    )))
            ->add('heureValidees','integer')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\AppBundle\Entity\CoursPif'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'edspim_bundle_appbundle_courspif';
    }
}
