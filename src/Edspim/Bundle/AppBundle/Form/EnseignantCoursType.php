<?php

namespace Edspim\Bundle\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnseignantCoursType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('enseignant', 'entity', array(
                    'class' => 'EdspimAppBundle:Enseignant',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control input-sm'
                    )
                ))
                ->add('heureCours', 'integer', array(
                    'attr' => array(
                        'class' => 'form-control input-sm'
                    )
                ))
                ->add('heureTD', 'integer', array(
                    'attr' => array(
                        'class' => 'form-control input-sm'
                    )
                ))
                ->add('heureTP', 'integer', array(
                    'attr' => array(
                        'class' => 'form-control input-sm'
                    )
                ))
                ->add('estResponsable', 'checkbox', array(
                    'required' => false,
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\AppBundle\Entity\EnseignantCours'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'edspim_bundle_appbundle_enseignantcours';
    }

}
