<?php

namespace Edspim\Bundle\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CoursModifType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titre', 'text', array(
                    'label' => 'cours.titre',
                    'max_length' => 50,
                ))
                ->add('acronyme', 'text', array(
                    'label' => 'cours.acronyme',
                    'max_length' => 10,
                ))
                ->add('typeCours', 'choice', array(
                    'label' => 'cours.typeCours.nom',
                    'choices' => array(
                        "cours.typeCours.formation" => "cours.typeCours.formation",
                        "cours.typeCours.boite" => "cours.typeCours.boite",
                        "cours.typeCours.coursED" => "cours.typeCours.coursED",
                        "cours.typeCours.tutoCongres" => "cours.typeCours.tutoCongres",
                        "cours.typeCours.ecoleEte" => "cours.typeCours.ecoleEte",
                        "cours.typeCours.autre" => "cours.typeCours.autre",
                    )
                ))
                ->add('oral', 'checkbox', array('required' => false))
                ->add('presentationOral', 'checkbox', array('required' => false))
                ->add('ecrit', 'checkbox', array('required' => false))
                ->add('ecritOral', 'checkbox', array('required' => false))
                ->add('rapportProjet', 'checkbox', array('required' => false))
                ->add('finalEcrit', 'checkbox', array('required' => false))
                ->add('multiple', 'checkbox', array('required' => false))
                ->add('autre', 'checkbox', array('required' => false))
                ->add('anneeFormation', 'text', array('required' => false))
                ->add('anneeDebut', 'text', array('required' => false))
                ->add('anneeFin', 'text', array('required' => false))
                ->add('frequence', 'text', array('required' => false))
                ->add('maxNbParticipants', 'integer')
                ->add('salleReservee', 'text', array('required' => false))
                ->add('batiment', 'text', array('required' => false))
                ->add('adresse', 'text', array('required' => false))
                ->add('lieu', 'text')
                ->add('acquis', 'textarea', array('required' => false))
                ->add('contenu', 'textarea', array('required' => false))
                ->add('commentaireED', 'textarea', array('required' => false))
                ->add('connaissancesDemandees', 'textarea', array('required' => false))
                ->add('enseignantCours', 'collection', array(
                    'type' => new EnseignantCoursType(),
                    'allow_add' => true,
                    'by_reference' => false,
                    'required' => false,
                ))
                ->add('supprimer', 'submit')
                ->add('brouillon', 'submit')
                ->add('archiver', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\AppBundle\Entity\Cours'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'edspim_bundle_appbundle_coursmodif';
    }

}
