<?php

namespace Edspim\Bundle\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CoursHorsSPIMType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('intitule', 'text')
                ->add('lieu', 'text')
                ->add('annee', 'integer')
                ->add('typeFormation', 'choice', array(
                    'choices' => array(
                        "cours.typeCours.formation" => "cours.typeCours.formation",
                        "cours.typeCours.boite" => "cours.typeCours.boite",
                        "cours.typeCours.coursED" => "cours.typeCours.coursED",
                        "cours.typeCours.tutoCongres" => "cours.typeCours.tutoCongres",
                        "cours.typeCours.ecoleEte" => "cours.typeCours.ecoleEte",
                        "cours.typeCours.autre" => "cours.typeCours.autre",
                    )
                ))
                ->add('categorieSouhaitee', 'choice', array(
                    'choices' => array(
                        "cours.typeCours.formation" => "cours.typeCours.formation",
                        "cours.typeCours.boite" => "cours.typeCours.boite",
                        "cours.typeCours.science" => "cours.typeCours.science",
                    )
                ))
                ->add('nbHeures', 'integer')
                ->add('nbJours', 'integer')
                ->add('commentaire', 'textarea')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Edspim\Bundle\AppBundle\Entity\CoursHorsSPIM'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'edspim_bundle_appbundle_courshorsspim';
    }

}
