<?php

namespace Edspim\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Edspim\Bundle\AppBundle\Entity\Cours;
use Edspim\Bundle\AppBundle\Entity\Seance;
use Edspim\Bundle\AppBundle\Entity\Doctorant;

class DefaultController extends Controller {

    /**
     * Index de l'application
     * Redirige vers le portail si déjà connecté
     * 
     * @return type
     */
    public function indexAction() {
        if (is_object($this->getUser())) {
            return $this->redirect($this->generateUrl('edspim_portail'));
        }
        return $this->render('EdspimAppBundle:Default:index.html.twig');
    }

    /**
     * Change la langue du site
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param type $lang soit fr, soit en
     * @return type
     */
    public function langueAction(Request $request, $lang) {
        if ("fr" === $lang) {
            $this->get('session')->set('_locale', 'fr');
            $request->setLocale("fr");
        } elseif ("en" === $lang) {
            $this->get('session')->set('_locale', 'en');
            $request->setLocale("en");
        }
        return $this->redirect($this->generateUrl("edspim_index"));
    }

    /**
     * Portail d'un utilisateur connecté
     * 
     * @return type
     * @throws type
     */
    public function portailAction() {

        /*
         * Si Administratif :
         * Nombre de cours, nombre d'enseignants, nombre de doctorants, 
         * Nombre d'activation de compte, nombre de cours Hors SPIM demandé
         */
        if ($this->get('security.context')->isGranted('ROLE_ED')) {
            $em = $this->getDoctrine()->getManager();
            $nbCours = $em->getRepository('EdspimAppBundle:Cours')->findNbCours(array(
                'cours.statutCours.archive',
                'cours.statutCours.inscription',
                'cours.statutCours.ouvert'
            ));
            $nbEnseignants = $em->getRepository('EdspimAppBundle:Enseignant')->findNbEnseignant();
            $nbDoctorants = $em->getRepository('EdspimAppBundle:Doctorant')->findNbDoctorant();
            $nbUser = count($em->getRepository('EdspimUserBundle:User')->findBy(array('enabled' => false)));
            $nbCoursHS = $em->getRepository('EdspimAppBundle:CoursHorsSPIM')->findNbCoursHS();

            return $this->render('EdspimAppBundle:Default:portail_ed.html.twig', array(
                        'nbCours' => $nbCours,
                        'nbEnseignants' => $nbEnseignants,
                        'nbDoctorants' => $nbDoctorants,
                        'nbUser' => $nbUser,
                        'nbCoursHS' => $nbCoursHS,
            ));
        }
        /*
         * Si doctorant
         */ elseif ($this->get('security.context')->isGranted('ROLE_DOCTORANT')) {
            return $this->render('EdspimAppBundle:Default:portail_doctorant.html.twig');
        }
        /*
         * Si enseignant
         */ elseif ($this->get('security.context')->isGranted('ROLE_ENSEIGNANT')) {
            return $this->render('EdspimAppBundle:Default:portail_enseignant.html.twig');
        }
        /*
         * Si aucun de ces droits
         */
        throw $this->createNotFoundException('Aucun droit attribué');
    }

    /**
     * Modification d'un profil
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return type
     * @throws type
     */
    public function modificationAction(Request $request) {

        if ($this->get('security.context')->isGranted('ROLE_ED')) {
            $ed = $this->getUser()->getEd();
            $form = $this->createForm(new \Edspim\Bundle\UserBundle\Form\EDModifType(), $ed);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($ed);
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_portail"));
            }
            return $this->render('EdspimAppBundle:Default:modification.html.twig', array(
                        "form" => $form->createView(),
                        "ed" => $ed,
            ));
        } elseif ($this->get('security.context')->isGranted('ROLE_DOCTORANT')) {
            $doctorant = $this->getUser()->getDoctorant();
            $form = $this->createForm(new \Edspim\Bundle\UserBundle\Form\DoctorantModifType(), $doctorant);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($doctorant);
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_portail"));
            }
            return $this->render('EdspimAppBundle:Default:modification.html.twig', array(
                        "form" => $form->createView(),
                        "doctorant" => $doctorant,
            ));
        } elseif ($this->get('security.context')->isGranted('ROLE_ENSEIGNANT')) {
            $enseignant = $this->getUser()->getEnseignant();
            $form = $this->createForm(new \Edspim\Bundle\UserBundle\Form\EnseignantModifType(), $enseignant);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($enseignant);
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_portail"));
            }
            return $this->render('EdspimAppBundle:Default:modification.html.twig', array(
                        "form" => $form->createView(),
                        "enseignant" => $enseignant,
            ));
        }
        throw $this->createNotFoundException('Aucun droit attribué');
    }

    /**
     * Liste des doctorants
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return type
     */
    public function doctorantListeAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()->add('recherche', 'text')->getForm();

        $form->handleRequest($request);
        $recherche = null;
        if ($form->isValid() && strlen($form->get('recherche')->getData()) > 1) {
            $recherche = $form->get('recherche')->getData();
        }
        $docListe = $em->getRepository('EdspimAppBundle:Doctorant')
                ->findListeDoctorant(null, 20, $recherche);

        return $this->render('EdspimAppBundle:Default:doctorantListe.html.twig', array(
                    'docListe' => $docListe,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Retourne le profil d'un doctorant
     * 
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return type
     */
    public function doctorantAction(Doctorant $doctorant) {

        return $this->render('EdspimAppBundle:Default:doctorant.html.twig', array(
                    'doctorant' => $doctorant,
        ));
    }

    /**
     * Retourne des cours disponibles
     * 
     * @param Symfony\Component\HttpFoundation\Request $request
     * @return type
     */
    public function coursListeAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
                ->add('rechercheParNom', 'text')
                ->add('rechercheParVille', 'text')
                ->getForm();
        $form->handleRequest($request);
        $rechercheParNom = null;
        $rechercheParVille = null;
        if ($form->isValid()) {
            if (strlen($form->get('rechercheParNom')->getData()) > 1) {
                $rechercheParNom = $form->get('rechercheParNom')->getData();
            }
            if (strlen($form->get('rechercheParVille')->getData()) > 1) {
                $rechercheParVille = $form->get('rechercheParVille')->getData();
            }
        }
        $coursListe = $em->getRepository('EdspimAppBundle:Cours')
                ->findListeCours(array(
            'cours.statutCours.archive',
            'cours.statutCours.inscription',
            'cours.statutCours.ouvert'
                ), 20, $rechercheParNom, $rechercheParVille);

        return $this->render('EdspimAppBundle:Default:coursListe.html.twig', array(
                    'coursListe' => $coursListe,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Affiche le cours donné
     * 
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @return type
     * @throws type
     */
    public function coursAction(Cours $cours) {

        if (!in_array($cours->getStatutCours(), array('cours.statutCours.archive', 'cours.statutCours.inscription', 'cours.statutCours.ouvert'))) {
            throw $this->createNotFoundException("Le cours n'a pas le statut requis");
        }

        $estED = false;
        $estEnseignant = false;
        $estDoctorant = false;

        if ($this->get('security.context')->isGranted('ROLE_ED')) {
            $estED = true;
        } elseif ($this->get('security.context')->isGranted('ROLE_ENSEIGNANT')) {
            $enseignant = $this->getUser()->getEnseignant();
            foreach ($cours->getEnseignantCours() as $ensCours) {
                if ($enseignant === $ensCours->getEnseignant()) {
                    $estEnseignant = true;
                }
            }
        } elseif ($this->get('security.context')->isGranted('ROLE_DOCTORANT')) {
            $doctorant = $this->getUser()->getDoctorant();
            $docCours = $this->getDoctrine()->getManager()
                    ->getRepository('EdspimAppBundle:CoursDoctorant')
                    ->findOneBy(array(
                'doctorant' => $doctorant,
                'cours' => $cours,
                'statut' => 'coursdoc.statut.actif'));
            if (is_object($docCours)) {
                $estDoctorant = true;
            }
        }

        return $this->render('EdspimAppBundle:Default:cours.html.twig', array(
                    'cours' => $cours,
                    'estED' => $estED,
                    'estEnseignant' => $estEnseignant,
                    'estDoctorant' => $estDoctorant,
        ));
    }

    public function seanceDetailAction(Request $request, Seance $seance) {
        if ($request->isXmlHttpRequest()) {
            return $this->render("EdspimAppBundle:Default:seanceDetail.html.twig", array(
                        "seance" => $seance,
            ));
        }
    }

}
