<?php

namespace Edspim\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Edspim\Bundle\UserBundle\Entity\User;
use Edspim\Bundle\AppBundle\Entity\Doctorant;
use Edspim\Bundle\AppBundle\Entity\Enseignant;
use Edspim\Bundle\AppBundle\Entity\ED;
use Edspim\Bundle\AppBundle\Entity\Cours;
use Edspim\Bundle\AppBundle\Entity\Seance;
use Edspim\Bundle\AppBundle\Entity\CoursPif;
use Edspim\Bundle\AppBundle\Entity\CoursHorsSPIM;
use Edspim\Bundle\AppBundle\Entity\Commentaire;
use Doctrine\ORM\EntityRepository;

class EDController extends Controller {
    
    
    /**
     * Liste des enseignants de l'ED SPIM
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return type
     */
    public function enseignantListeAction(Request $request) {
        
        $form = $this->createFormBuilder()->add('recherche', 'text')->getForm();
        $form->handleRequest($request);
        $recherche = null;
        if ($form->isValid() && strlen($form->get('recherche')->getData()) > 1) {
            $recherche = $form->get('recherche')->getData();
        }
        $listeEnseignants = $this->getDoctrine()->getManager()
                ->getRepository('EdspimAppBundle:Enseignant')
                ->findListeEnseignant(null, 20, $recherche);
        return $this->render('EdspimAppBundle:ED:enseignantListe.html.twig', array(
                    'listeEnseignant' => $listeEnseignants,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Vue d'un enseignant
     * 
     * @param \Edspim\Bundle\AppBundle\Entity\Enseignant $enseignant
     * @return type
     */
    public function enseignantAction(Enseignant $enseignant) {

        return $this->render('EdspimAppBundle:ED:enseignant.html.twig', array(
                    'enseignant' => $enseignant,
        ));
    }
    
    /**
     * Liste les comptes qui ne sont pas activés
     * 
     * @return type
     */
    public function compteListeAction() {

        $comptes = $this->getDoctrine()->getManager()
                ->getRepository('EdspimUserBundle:User')
                ->findBy(array("enabled" => 0), null, 20);

        return $this->render("EdspimAppBundle:ED:compteListe.html.twig", array(
                    "comptes" => $comptes,
        ));
    }

    /**
     * Fait une action sur le compte (valider ou supprimer)
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\UserBundle\Entity\User $compte
     * @return type
     * @throws type
     */
    public function compteAction(Request $request, User $compte) {

        if (!$compte->isEnabled()) {
            $statut = $compte->getStatut();
            switch ($statut) {
                case "utilisateur.statut.doctorant" :
                    $reponse = $this->activationDoctorant($request, $compte);
                    break;
                case "utilisateur.statut.enseignant" :
                    $reponse = $this->activationEnseignant($request, $compte);
                    break;
                case "utilisateur.statut.ed" :
                    $reponse = $this->activationED($request, $compte);
                    break;
                default:
                    $reponse = $this->redirect($this->generateUrl("edspim_ed_compte_liste"));
                    break;
            }
            return $reponse;
        }
        throw $this->createNotFoundException();
    }
    
    /**
     * Ensemble des cours en brouillons et des propositions de cours
     * 
     * @return type
     */
    public function coursGestionAction() {

        $em = $this->getDoctrine()->getManager()
                ->getRepository('EdspimAppBundle:Cours');
        $brouillons = $em->findBy(array('statutCours' => 'cours.statutCours.brouillon', 'createur' => $this->getUser()));
        $coursProposition = $em->findBy(array('statutCours' => 'proposition'));

        return $this->render('EdspimAppBundle:ED:coursGestion.html.twig', array(
                    'mesBrouillons' => $brouillons,
                    'propositions' => $coursProposition,
        ));
    }
    
    /**
     * Permet d'ajouter un nouveau cours au sein de l'ED SPIM
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return type
     */
    public function coursAjoutAction(Request $request) {

        $cours = new Cours();
        $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\CoursAjoutType(), $cours);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cours->setCreateur($this->getUser());
            $cours->setDateModification(new \DateTime());
            $cours->setStatutCours("cours.statutCours.brouillon");
            $em->persist($cours);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_ed_cours_modifier", array('cours' => $cours->getId())));
        }

        return $this->render("EdspimAppBundle:ED:coursAjout.html.twig", array(
                    "form" => $form->createView(),
        ));
    }

    /**
     * Permet de modifier un cours
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @return type
     */
    public function coursModifAction(Request $request, Cours $cours) {

        $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\CoursModifType(), $cours);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cours->setDateModification(new \DateTime());
            if ($form->get('supprimer')->isClicked()) {
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_ed_cours_supprimer", array('cours' => $cours->getId())));
            } elseif ($form->get('brouillon')->isClicked()) {
                $cours->setStatutCours("cours.statutCours.brouillon");
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_ed_cours_gestion"));
            } else {
                $cours->setStatutCours("cours.statutCours.archive");
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_cours", array('cours' => $cours->getId())));
            }
        }

        return $this->render("EdspimAppBundle:ED:coursModif.html.twig", array(
                    "form" => $form->createView(),
        ));
    }

    /**
     * Permet de supprimer un cours
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @return type
     */
    public function coursSupprAction(Request $request, Cours $cours) {

        if (!in_array($cours->getStatutCours(), array('cours.statutCours.archive', 'cours.statutCours.brouillon'))) {
            return $this->createNotFoundException("Le cours n'a pas le statut requis");
        }

        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cours->setStatutCours("cours.statutCours.supprime");
            $cours->setDateModification(new \DateTime());
            $em->persist($cours);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_ed_cours_gestion"));
        }
        return $this->render("EdspimAppBundle:ED:coursSuppr.html.twig", array(
                    "form" => $form->createView(),
                    "cours" => $cours,
        ));
    }
    
    
    
    
    /**
     * Page de confirmation et de désactivation d'un doctorant
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant le doc en question
     * @return type
     * @throws type erreur 404
     */
    public function desactiverDocAction(Request $request, Doctorant $doctorant) {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("EdspimUserBundle:User")->findOneBy(array("doctorant" => $doctorant));
        if ($user && $user->isEnabled()) {
            $form = $this->createFormBuilder()->getForm();
            $form->handleRequest($request);
            if ($form->isValid()) {
                $doctorant->setStatutDoctorant("doctorant.statutDoctorant.clos");
                $user->setEnabled(false);
                $em->persist($doctorant);
                $em->persist($user);
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_doctorant", array("doctorant" => $doctorant->getId())));
            }
            return $this->render("EdspimAppBundle:ED:desactiverDoc.html.twig", array(
                        "form" => $form->createView(),
                        "doctorant" => $doctorant,
            ));
        }
        throw $this->createNotFoundException();
    }

    /**
     * Voir le PIF d'un doctorant
     * 
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return type
     */
    public function pifAction(Doctorant $doctorant) {
        return $this->render("EdspimAppBundle:ED:pif.html.twig", array(
                    "doctorant" => $doctorant,
        ));
    }

    /**
     * Activation d'un commentaire
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Commentaire $comm
     * @return type
     */
    public function activerCommAction(Request $request, Commentaire $comm) {
        $em = $this->getDoctrine()->getManager();
        $comm->setVisible(true);
        $id = $comm->getCours()->getId();
        $em->persist($comm);
        $em->flush();
        return $this->redirect($this->generateUrl("edspim_cours", array('cours' => $id)));
    }

    /**
     * Suppression d'un commentaire
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Commentaire $comm
     * @return type
     */
    public function supprimerCommAction(Request $request, Commentaire $comm) {
        $em = $this->getDoctrine()->getManager();
        $id = $comm->getCours()->getId();
        $em->remove($comm);
        $em->flush();
        return $this->redirect($this->generateUrl("edspim_cours", array('cours' => $id)));
    }

    

    /**
     * Suppression d'un CoursPIF
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\CoursPif $pif
     * @return type
     */
    public function supprimerPifAction(Request $request, CoursPif $pif) {

        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $id = $pif->getDoctorant()->getId();
            $em = $this->getDoctrine()->getManager();
            $em->remove($pif);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_doctorant", array('doctorant' => $id)));
        }
        return $this->render("EdspimAppBundle:ED:supprimerPif.html.twig", array(
                    "form" => $form->createView(),
                    "pif" => $pif,
        ));
    }
    
    public function ajouterPifAction(Request $request, Doctorant $doctorant) {
        $coursPif = new CoursPif();
        $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\CoursPifType(), $coursPif);
        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $coursPif->setDoctorant($doctorant);
            $coursPif->setCredential(uniqid("co3_", true));
            $em->persist($coursPif);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_doctorant", array('doctorant' => $doctorant->getId())));
        }
        return $this->render("EdspimAppBundle:ED:ajouterPif.html.twig", array(
                    "form" => $form->createView(),
                    "doctorant" => $doctorant,
        ));
    }
    
    
    public function modifierPifAction(Request $request, CoursPif $coursPif) {
        $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\CoursPifType(), $coursPif);
        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($coursPif);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_doctorant", array('doctorant' => $coursPif->getDoctorant()->getId())));
        }
        return $this->render("EdspimAppBundle:ED:ajouterPif.html.twig", array(
                    "form" => $form->createView(),
                    "doctorant" => $coursPif->getDoctorant(),
        ));
    }

    /**
     * Vue d'une séance
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Seance $seance
     * @return type
     * @throws type
     */
    public function seanceAction(Request $request, Seance $seance) {

        if ($request->isXmlHttpRequest()) {
            return $this->render("EdspimAppBundle:Default:seance.html.twig", array(
                        "seance" => $seance,
            ));
        }
        throw $this->createNotFoundException();
    }

    public function seanceAjoutAction(Request $request, Cours $cours) {

        if (in_array($cours->getStatutCours(), array('cours.statutCours.inscription', 'cours.statutCours.ouvert'))) {

            $seance = new Seance();
            $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\SeanceType(null, null, null, null), $seance);

            if ($request->isXmlHttpRequest()) {
                return $this->render("EdspimAppBundle:Default:seanceAjout.html.twig", array(
                            "form" => $form->createView(),
                            "idCours" => $cours->getId(),
                ));
            }

            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $cours->addSeance($seance);
                $em->persist($cours);
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_cours", array('cours' => $cours->getId())));
            }
        }
        throw $this->createNotFoundException();
    }

    /**
     * Change le statut du cours
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @param type $statut
     * @return type
     * @throws type
     */
    public function changerStatutAction(Request $request, Cours $cours, $statut) {

        $statutCours = $cours->getStatutCours();
        switch ($statut) {
            case "inscription" :
                if ("cours.statutCours.archive" === $statutCours) {
                    return $this->mettreEnInscription($request, $cours, $statut);
                }
                break;
            case "ouverture" :
                if ("cours.statutCours.inscription" === $statutCours) {
                    return $this->mettreEnOuverture($request, $cours, $statut);
                }
                break;
            case "archivage" :
                if ("cours.statutCours.ouvert" === $statutCours) {
                    return $this->mettreEnArchive($request, $cours, $statut);
                }
                break;
            case "annulation" :
                if ("cours.statutCours.inscription" === $statutCours) {
                    return $this->changerStatut($request, $cours, $statut);
                }
                break;
        }
        throw $this->createNotFoundException();
    }

    public function commentaireAction(Request $request, Cours $cours) {
        $em = $this->getDoctrine()->getManager();

        $listeCommentaires = $em->getRepository('EdspimAppBundle:Commentaire')->findBy(array(
            'cours' => $cours
        ));

        return $this->render('EdspimAppBundle:ED:cours_commentaire.html.twig', array(
                    'listeCommentaires' => $listeCommentaires,
                    'cours' => $cours,
        ));
    }

    /**
     * Liste des demandes de cours Hors SPIM
     * 
     * @return type
     */
    public function coursHorsSpimListeAction() {
        $em = $this->getDoctrine()->getManager();

        $coursHorsSPIMListe = $em->getRepository("EdspimAppBundle:CoursHorsSPIM")->findBy(array(
            "statut" => "demande.statut.attente",
                ), array('dateDemande' => 'DESC'), 20);
        return $this->render("EdspimAppBundle:ED:coursHorsSPIMListe.html.twig", array(
                    "coursHorsSPIMListe" => $coursHorsSPIMListe,
        ));
    }

    public function coursHorsSpimAction(Request $request, CoursHorsSPIM $coursHS) {

        $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\CoursHorsSPIMValidType(), $coursHS);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($form->get('refuser')->isClicked()) {
                $coursHS->setStatut("demande.statut.refus");
            } else {
                $coursHS->setStatut("demande.statut.accepte");
                $coursPif = new CoursPif();
                $coursPif->setNomCoursPif($coursHS->getIntitule());
                $coursPif->setTypeCours($coursHS->getTypeFormation());
                $coursPif->setDoctorant($coursHS->getDoctorant());
                $coursPif->setHeureValidees(0);
                $coursPif->setCredential($coursHS->getCredential());
                $coursPif->setAnneePif($coursHS->getAnnee());
                $em->persist($coursPif);
            }
            $coursHS->setDateValidation(new \DateTime());
            $em->persist($coursHS);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_ed_coursHS_liste"));
        }
        return $this->render("EdspimAppBundle:ED:coursHorsSPIM.html.twig", array(
                    "form" => $form->createView(),
                    "coursHS" => $coursHS,
        ));
    }

    public function seancePresenceAction(Request $request, Seance $seance) {

        if ($seance->getCours()->getStatutCours() === "cours.statutCours.ouvert") {
            $form = $this->createFormBuilder()
                            ->add('sontPresents', 'entity', array(
                                'class' => "EdspimAppBundle:SeanceDoctorant",
                                'multiple' => true,
                                'expanded' => true,
                                'query_builder' => function(EntityRepository $er) use ($seance) {
                            return $er->createQueryBuilder('s')
                                    ->where("s.seance = ?1")
                                    ->andWhere("s.estPresent = true")
                                    ->setParameter(1, $seance->getId());
                        }))
                            ->add('sontAbsents', 'entity', array(
                                'class' => "EdspimAppBundle:SeanceDoctorant",
                                'multiple' => true,
                                'expanded' => true,
                                'query_builder' => function(EntityRepository $er) use ($seance) {
                            return $er->createQueryBuilder('s')
                                    ->where("s.seance = ?1")
                                    ->andWhere("s.estPresent = false")
                                    ->setParameter(1, $seance->getId());
                        }))->getForm();
            $form->handleRequest($request);
            if ($request->isXmlHttpRequest()) {
                return $this->render("EdspimAppBundle:ED:seancePresence.html.twig", array(
                            "form" => $form->createView(),
                            "seance" => $seance
                ));
            } elseif ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $absentsToPresents = $form["sontAbsents"]->getData();
                foreach ($absentsToPresents as $seanceDoc) {
                    $seanceDoc->setEstPresent(true);
                }
                $seance->setPresenceED(true);
                $em->persist($seance);
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_cours", array("cours" => $seance->getCours()->getId())));
            }
        }

        return $this->createNotFoundException();
    }

    /**
     * Met le cours en inscription
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @param type $statut
     * @return type
     */
    private function mettreEnInscription(Request $request, Cours $cours, $statut) {

        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cours->setStatutCours("cours.statutCours.inscription");
            $em->persist($cours);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_cours", array("cours" => $cours->getId())));
        }
        return $this->render("EdspimAppBundle:ED:changerStatut.html.twig", array(
                    "form" => $form->createView(),
                    "cours" => $cours,
                    "statut" => $statut,
        ));
    }

    private function mettreEnOuverture(Request $request, Cours $cours, $statut) {

        $form = $this->createFormBuilder()->add('coursDoctorant', 'entity', array(
                    'class' => 'EdspimAppBundle:CoursDoctorant',
                    'multiple' => true,
                    'expanded' => true,
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                                ->where("u.statut = 'coursdoc.statut.inscription'");
            },))->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $docInscrits = $form["coursDoctorant"]->getData();
            foreach ($docInscrits as $doc) {
                $doc->setStatut('coursdoc.statut.actif');
                $em->persist($doc);
                $cp = new CoursPif();
                $cp->setNomCoursPif($cours->getAcronyme() . " " . $cours->getTitre());
                $cp->setTypeCours($cours->getTypeCours());
                $cp->setAnneePif($doc->getDoctorant()->getAnneeDoctorant());
                $cp->setCredential($doc->getCredential());
                $cp->setDoctorant($doc->getDoctorant());
                $em->persist($cp);
            }
            $em->flush();
            $aSupprimer = $em->getRepository('EdspimAppBundle:CoursDoctorant')->findBy(array(
                'cours' => $cours,
                'statut' => 'coursdoc.statut.inscription'
            ));
            foreach ($aSupprimer as $delete) {
                $em->remove($delete);
            }
            $cours->setStatutCours("cours.statutCours.ouvert");
            $em->persist($cours);
            $em->flush();

            return $this->redirect($this->generateUrl("edspim_cours", array("cours" => $cours->getId())));
        }
        return $this->render("EdspimAppBundle:ED:changerStatut.html.twig", array(
                    "form" => $form->createView(),
                    "cours" => $cours,
                    "statut" => $statut,
        ));
    }

    private function mettreEnArchive(Request $request, Cours $cours, $statut) {

        $form = $this->createFormBuilder()->add('coursDoctorant', 'entity', array(
                    'class' => 'EdspimAppBundle:CoursDoctorant',
                    'multiple' => true,
                    'expanded' => true,
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('u')
                                ->where("u.statut = 'coursdoc.statut.actif'");
            },))->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $ontReussit = $form['coursDoctorant']->getData();
            //pour chaque doctorant coché
            foreach ($ontReussit as $reussit) {
                //Le cours est fini, on le persist
                $reussit->setStatut("coursdoc.statut.fini");
                $em->persist($reussit);
                //On recupere le coursPif associé et on le MAJ
                $coursPif = $em->getRepository("EdspimAppBundle:CoursPif")
                        ->findOneBy(array('credential' => $reussit->getCredential()));
                $test = $em->getRepository("EdspimAppBundle:SeanceDoctorant")
                        ->findHeuresValidees($reussit->getDoctorant()->getId(), $cours->getId());
                $coursPif->setHeureValidees(intval($test[0]));
                $em->persist($coursPif);
            }
            $em->flush();
            $aSupprimer = $em->getRepository("EdspimAppBundle:CoursDoctorant")
                    ->findBy(array("cours" => $cours, "statut" => "coursdoc.statut.ouvert"));
            foreach ($aSupprimer as $supp) {
                $coursPif = $em->getRepository("EdspimAppBundle:CoursPif")
                        ->findOneBy(array('credential' => $supp->getCredential()));
                $em->remove($coursPif);
                $em->remove($supp);
            }
            $cours->setStatutCours("cours.statutCours.archive");
            $cours->setSession($cours->getSession() + 1);
            $em->persist($cours);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_cours", array("cours" => $cours->getId())));
        }
        return $this->render("EdspimAppBundle:ED:changerStatut.html.twig", array(
                    "form" => $form->createView(),
                    "cours" => $cours,
                    "statut" => $statut,
        ));
    }

    /**
     * Activation du compte doctorant
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\UserBundle\Entity\User $compte
     * @return type
     */
    private function activationDoctorant(Request $request, User $compte) {

        $doctorant = new Doctorant();
        $doctorant->setNom($compte->getNom());
        $doctorant->setPrenom($compte->getPrenom());
        $doctorant->setEmail($compte->getEmail());

        $form = $this->createForm(new \Edspim\Bundle\UserBundle\Form\DoctorantType(), $doctorant);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($form->get('valider')->isClicked()) {
                $compte->setRoles(array('ROLE_DOCTORANT'));
                $compte->setEnabled(true);
                $compte->setDoctorant($doctorant);
                $em->persist($compte);
                $em->flush();
                $this->sendMailInscription("activation", $compte);
                $this->get('session')->getFlashBag()->add(
                        'compte', 'Le compte a été validé'
                );
            } else if ($form->get('supprimer')->isClicked()) {
                $this->sendMailInscription("suppression", $compte);
                $em->remove($compte);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                        'compte', 'Le compte a été supprimé'
                );
            }
            return $this->redirect($this->generateUrl("edspim_ed_compte_liste"));
        }
        return $this->render("EdspimAppBundle:ED:compte.html.twig", array(
                    "statut" => $compte->getStatut(),
                    "compte" => $compte,
                    "form" => $form->createView(),
        ));
    }

    /**
     * Activation du compte Enseignant
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\UserBundle\Entity\User $compte
     * @return type
     */
    private function activationEnseignant(Request $request, User $compte) {

        $em = $this->getDoctrine()->getManager();
        $enseignant = new Enseignant();
        $enseignant->setNom($compte->getNom());
        $enseignant->setPrenom($compte->getPrenom());
        $enseignant->setEmail($compte->getEmail());

        $form = $this->createForm(new \Edspim\Bundle\UserBundle\Form\EnseignantType(), $enseignant);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($form->get('valider')->isClicked()) {
                $compte->setRoles(array('ROLE_ENSEIGNANT'));
                $compte->setEnabled(true);
                $compte->setEnseignant($enseignant);
                $em->persist($compte);
                $em->flush();
                $this->sendMailInscription("activation", $compte);
                $this->get('session')->getFlashBag()->add(
                        'compte', 'Le compte a été validé'
                );
            } else if ($form->get('supprimer')->isClicked()) {
                $this->sendMailInscription("suppression", $compte);
                $em->remove($compte);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                        'compte', 'Le compte a été supprimé'
                );
            }
            return $this->redirect($this->generateUrl("edspim_ed_compte_liste"));
        }

        return $this->render("EdspimAppBundle:ED:compte.html.twig", array(
                    "statut" => $compte->getStatut(),
                    "compte" => $compte,
                    "form" => $form->createView(),
        ));
    }

    /**
     * Activation du compte Administratif
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Edspim\Bundle\UserBundle\Entity\User $compte
     * @return type
     */
    private function activationED(Request $request, User $compte) {

        $em = $this->getDoctrine()->getManager();
        $ed = new ED();
        $ed->setNom($compte->getNom());
        $ed->setPrenom($compte->getPrenom());
        $ed->setEmail($compte->getEmail());
        $form = $this->createForm(new \Edspim\Bundle\UserBundle\Form\EDType(), $ed);

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($form->get('valider')->isClicked()) {
                $compte->setRoles(array('ROLE_ED'));
                $compte->setEnabled(true);
                $compte->setEd($ed);
                $em->persist($compte);
                $em->flush();
                $this->sendMailInscription("activation", $compte);
                $this->get('session')->getFlashBag()->add(
                        'compte', 'Le compte a été validé'
                );
            } else if ($form->get('supprimer')->isClicked()) {
                $this->sendMailInscription("suppression", $compte);
                $em->remove($compte);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                        'compte', 'Le compte a été supprimé'
                );
            }
            return $this->redirect($this->generateUrl("edspim_ed_compte_liste"));
        }

        return $this->render("EdspimAppBundle:ED:compte.html.twig", array(
                    "statut" => $compte->getStatut(),
                    "compte" => $compte,
                    "form" => $form->createView(),
        ));
    }

    /**
     * Envoi du mail a l'inscription
     * 
     * @param type $type
     * @param \Edspim\Bundle\UserBundle\Entity\User $compte
     * @param type $mdp
     */
    private function sendMailInscription($type, User $compte, $mdp = NULL) {

        switch ($type) {
            case "activation" :
                $message = \Swift_Message::newInstance()
                        ->setSubject('Activation compte ED SPIM')
                        ->setFrom('send@example.com')
                        ->setTo($compte->getEmail())
                        ->setBody($this->renderView('EdspimAppBundle:Default:mail_activation.html.twig', array(
                            'username' => $compte->getUsername(),
                            'statut' => $compte->getStatut())))
                ;
                break;
            case "suppression" :
                $message = \Swift_Message::newInstance()
                        ->setSubject('Supression compte ED SPIM')
                        ->setFrom('send@example.com')
                        ->setTo($compte->getEmail())
                        ->setBody($this->renderView('EdspimAppBundle:Default:mail_suppression.html.twig', array(
                            'username' => $compte->getUsername(),
                        )))
                ;
                break;

            default :
                $message = null;
        }
        if (null !== $message) {
            $this->get('mailer')->send($message);
        }
    }

}
