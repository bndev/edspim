<?php

namespace Edspim\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use Edspim\Bundle\AppBundle\Entity\Cours;
use Edspim\Bundle\AppBundle\Entity\Seance;
use Edspim\Bundle\AppBundle\Entity\SeanceDoctorant;

class EnseignantController extends Controller {

    public function commentaireAction(Request $request, Cours $cours) {
        $em = $this->getDoctrine()->getManager();
        $listeCommentaires = $em->getRepository('EdspimAppBundle:Commentaire')->findBy(array(
            'cours' => $cours,
            'visible' => true,
        ));

        return $this->render('EdspimAppBundle:Enseignant:cours_commentaire.html.twig', array(
                    'listeCommentaires' => $listeCommentaires,
                    'cours' => $cours,
        ));
    }

    public function seancePresenceAction(Request $request, Seance $seance) {

        $estEnseignant = false;
        $enseignant = $this->getUser()->getEnseignant();
        foreach ($seance->getCours()->getEnseignantCours() as $ensCours) {
            if ($enseignant === $ensCours->getEnseignant()) {
                $estEnseignant = true;
            }
        }
        if ($estEnseignant && $seance->getCours()->getStatutCours() === "cours.statutCours.ouvert") {
            $form = $this->createFormBuilder()
                            ->add('doctorants', 'entity', array(
                                'class' => "EdspimAppBundle:CoursDoctorant",
                                'multiple' => true,
                                'expanded' => true,
                                'query_builder' => function(EntityRepository $er) use ($seance) {
                            return $er->createQueryBuilder('d')
                                    ->where("d.statut = ?1")
                                    ->andWhere("d.cours = ?2")
                                    ->setParameter(1, "coursdoc.statut.actif")
                                    ->setParameter(2, $seance->getCours()->getId());
                        }))->getForm();
            $form->handleRequest($request);
            if ($request->isXmlHttpRequest()) {
                return $this->render("EdspimAppBundle:Enseignant:seancePresence.html.twig", array(
                    "form" => $form->createView(), 
                    "seance" => $seance
                ));
                
            } elseif ($form->isValid()) {
                
                $em = $this->getDoctrine()->getManager();
                $coursDoc = $em->getRepository('EdspimAppBundle:CoursDoctorant')->findBy(array(
                    'cours' => $seance->getCours(),
                    'statut' => 'coursdoc.statut.actif',
                ));
                $listeDoctorants = $form["doctorants"]->getData();
                foreach($coursDoc as $cd) {
                    $seanceDoc = new SeanceDoctorant();
                    $seanceDoc->setDoctorant($cd->getDoctorant());
                    $seanceDoc->setSeance($seance);
                    foreach($listeDoctorants as $ld) {
                        if($ld === $cd) {
                            $seanceDoc->setEstPresent(true);
                        }
                    }
                    $em->persist($seanceDoc);
                }
                $seance->setPresenceEnseignant(true);
                $em->persist($seance);
                $em->flush();
                return $this->redirect($this->generateUrl("edspim_cours", array("cours" => $seance->getCours()->getId())));
            }
        }

        return $this->createNotFoundException();
    }

}
