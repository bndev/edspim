<?php

namespace Edspim\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Edspim\Bundle\AppBundle\Entity\Cours;
use Edspim\Bundle\AppBundle\Entity\Commentaire;
use Edspim\Bundle\AppBundle\Entity\CoursHorsSPIM;

class DoctorantController extends Controller {

    /**
     * Liste des inscriptions possibles
     * 
     * @return type
     */
    public function inscriptionListeAction() {

        $doctorant = $this->getUser()->getDoctorant();
        $em = $this->getDoctrine()->getManager();

        $coursInscription = $em->getRepository('EdspimAppBundle:Cours')
                ->findByStatutCours("cours.statutCours.inscription");
        $docCours = $em->getRepository('EdspimAppBundle:Cours')
                ->findCoursByDoctorant($doctorant->getId());

        $coursNonInscrit = array();
        $coursInscrit = array();
        foreach ($coursInscription as $cours) {
            if (!in_array($cours, $docCours)) {
                $coursNonInscrit[] = $cours;
            }
        }
        foreach ($docCours as $cours) {
            if ("cours.statutCours.inscription" === $cours->getStatutCours()) {
                $coursInscrit[] = $cours;
            }
        }

        return $this->render('EdspimAppBundle:Doctorant:inscriptionListe.html.twig', array(
                    "cours" => $coursNonInscrit,
                    "mesCours" => $coursInscrit,
        ));
    }

    public function inscriptionAction(Request $request, Cours $cours) {

        $doctorant = $this->getUser()->getDoctorant();

        $em = $this->getDoctrine()->getEntityManager();

        $docCours = $em->getRepository('EdspimAppBundle:Cours')
                ->findCoursByDoctorant($doctorant->getId());

        if (!in_array($cours, $docCours) && "cours.statutCours.inscription" === $cours->getStatutCours()) {

            $form = $this->createFormBuilder()->getForm();
            $form->handleRequest($request);
            if ($form->isValid()) {
                $coursDoctorant = new \Edspim\Bundle\AppBundle\Entity\CoursDoctorant();
                $coursDoctorant->setDoctorant($doctorant);
                $coursDoctorant->setCours($cours);
                $em->persist($coursDoctorant);
                $em->flush();
                if($cours->getMaxNbParticipants() == count($cours->getCoursDoctorantInscription())) {
                    $this->envoiMailFull();
                }
                return $this->redirect($this->generateUrl('edspim_doctorant_inscription_liste'));
            }

            return $this->render('EdspimAppBundle:Doctorant:inscription.html.twig', array(
                        'form' => $form->createView(),
                        'cours' => $cours
            ));
        }
        throw $this->createNotFoundException('Impossible de s\'inscrire au cours');
    }

    public function desinscriptionAction(Request $request, Cours $cours) {

        if ("cours.statutCours.inscription" === $cours->getStatutCours()) {

            $em = $this->getDoctrine()->getEntityManager();
            $docCours = $em->getRepository('EdspimAppBundle:CoursDoctorant')
                    ->findOneBy(array('cours' => $cours, 'doctorant' => $this->getUser()->getDoctorant()));

            if (is_object($docCours)) {
                $form = $this->createFormBuilder()->getForm();
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $em->remove($docCours);
                    $em->flush();
                    return $this->redirect($this->generateUrl('edspim_doctorant_inscription_liste'));
                }

                return $this->render('EdspimAppBundle:Doctorant:desinscription.html.twig', array(
                            'form' => $form->createView(),
                            'cours' => $cours,
                ));
            }
        }
        throw $this->createNotFoundException('Impossible de se désinscrire du cours');
    }

    public function commentaireAction(Request $request, Cours $cours) {

        $em = $this->getDoctrine()->getEntityManager();
       
        $commentaire = new Commentaire();
        $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\CommentaireType(), $commentaire);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $commentaire->setDoctorant($this->getUser()->getDoctorant());
            $commentaire->setCours($cours);
            $em->persist($commentaire);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_cours", array("cours" => $cours->getId())));
        }
        
        $listeCommentaires = $em->getRepository('EdspimAppBundle:Commentaire')->findBy(array(
            'doctorant' => $this->getUser()->getDoctorant(),
            'cours' => $cours
        ));
        
        return $this->render('EdspimAppBundle:Doctorant:cours_commentaire.html.twig', array(
                    'form' => $form->createView(),
                    'listeCommentaires' => $listeCommentaires,
                    'cours' => $cours,
        ));
    }
    
    public function coursHorsSPIMListeAction() {
        
        $em = $this->getDoctrine()->getManager();
        $coursHorsSPIMListe = $em->getRepository("EdspimAppBundle:CoursHorsSPIM")->findBy(array('doctorant' => $this->getUser()->getDoctorant()));
        return $this->render("EdspimAppBundle:Doctorant:coursHorsSPIMListe.html.twig", array(
            "coursHorsSPIMListe" => $coursHorsSPIMListe,
        ));
    }
    
    public function coursHorsSPIMNouveauAction(Request $request) {
        
        $coursHorsSPIM = new CoursHorsSPIM();
        $form = $this->createForm(new \Edspim\Bundle\AppBundle\Form\CoursHorsSPIMType(), $coursHorsSPIM);
        $form->handleRequest($request);
        
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $coursHorsSPIM->setDoctorant($this->getUser()->getDoctorant());
            $coursHorsSPIM->setStatut("demande.statut.attente");
            $em->persist($coursHorsSPIM);
            $em->flush();
            return $this->redirect($this->generateUrl("edspim_doctorant_coursHorsSPIM_liste"));
        }
        
        return $this->render("EdspimAppBundle:Doctorant:coursHorsSPIMNouveau.html.twig", array(
            "form" => $form->createView(),
        ));
        
    }
    
    public function coursHorsSPIMAction(CoursHorsSPIM $coursHS) {
        
        return $this->render("EdspimAppBundle:Doctorant:coursHorsSPIM.html.twig", array(
            "coursHorsSPIM" => $coursHS,
        ));
        
    }
    
    public function pifAction() {
        
        return $this->render("EdspimAppBundle:Doctorant:pif.html.twig", array(
            "doctorant" => $this->getUser()->getDoctorant(),
        ));
    }
    
    public function pifImpressionAction() {
        return $this->render("EdspimAppBundle:Doctorant:pifImpression.html.twig", array(
            "doctorant" => $this->getUser()->getDoctorant(),
        ));
    }
    
    public function calendrierAction() {
        
        $doctorant = $this->getUser()->getDoctorant();
        $em = $this->getDoctrine()->getManager();
        $seancesListe = $em->getRepository("EdspimAppBundle:Seance")->findCalendrier($doctorant->getId());
        
        return $this->render("EdspimAppBundle:Doctorant:calendrier.html.twig", array(
            "seancesListe" => $seancesListe,
        ));
    }

    public function envoiMailFull(Cours $cours) {
        
        $message = \Swift_Message::newInstance()
                        ->setSubject('Cours '.$cours->getTitre().' complet')
                        ->setFrom('send@example.com')
                        ->setTo($cours->getCreateur()->getEmail())
                        ->setBody($this->renderView('EdspimAppBundle:ED:mail_courscomplet.html.twig', array(
                            'cours' => $cours,
                            )))
                ;
        if (null !== $message) {
            $this->get('mailer')->send($message);
        }
    }

}
