<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Doctorant
 *
 * @ORM\Table("Doctorant")
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\DoctorantRepository")
 */
class Doctorant {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $prenom;

    /**
     * @var email
     *
     * @ORM\Column(name="email", type="string", length=75)
     * 
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="labDpt", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $labDpt;

    /**
     * @var string
     *
     * @ORM\Column(name="statutDoctorant", type="string", length=50)
     * 
     * @Assert\Choice(choices = 
     *      {   "doctorant.statutDoctorant.actif", 
     *          "doctorant.statutDoctorant.fini", 
     *          "doctorant.statutDoctorant.clos", 
     *      }, 
     *      message = "Choisissez un statut valide.")
     */
    private $statutDoctorant;

    /**
     * @var integer
     *
     * @ORM\Column(name="anneeDoctorant", type="integer")
     * 
     * @Assert\Range(
     *      min = 1,
     *      max = 3,
     *      minMessage = "Année 1 minimum",
     *      maxMessage = "Année 3 maximum"
     * )
     */
    private $anneeDoctorant;

    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\CoursDoctorant", mappedBy="doctorant")
     */
    protected $coursDoctorant;

    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\CoursPif", mappedBy="doctorant")
     */
    private $coursPif;
    
    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\SeanceDoctorant", mappedBy="doctorant")
     */
    private $seanceDoctorant;

    /**
     * Fonction standard toString
     * 
     * @return string nom et prénom
     */
    public function __toString() {
        return $this->prenom . " " . $this->nom;
    }

    public function getAnneeToString() {
        $annee = '';
        switch ($this->anneeDoctorant) {
            case 1 :
                $annee = 'doctorant.1annee';
                break;
            case 2 :
                $annee = 'doctorant.2annee';
                break;
            case 3:
                $annee = 'doctorant.3annee';
                break;
        }
        return $annee;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Doctorant
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Doctorant
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set labDpt
     *
     * @param string $labDpt
     * @return Doctorant
     */
    public function setLabDpt($labDpt) {
        $this->labDpt = $labDpt;

        return $this;
    }

    /**
     * Get labDpt
     *
     * @return string 
     */
    public function getLabDpt() {
        return $this->labDpt;
    }

    /**
     * Set statutDoctorant
     *
     * @param string $statutDoctorant
     * @return Doctorant
     */
    public function setStatutDoctorant($statutDoctorant) {
        $this->statutDoctorant = $statutDoctorant;

        return $this;
    }

    /**
     * Get statutDoctorant
     *
     * @return string 
     */
    public function getStatutDoctorant() {
        return $this->statutDoctorant;
    }

    /**
     * Set anneeDoctorant
     *
     * @param integer $anneeDoctorant
     * @return Doctorant
     */
    public function setAnneeDoctorant($anneeDoctorant) {
        $this->anneeDoctorant = $anneeDoctorant;

        return $this;
    }

    /**
     * Get anneeDoctorant
     *
     * @return integer 
     */
    public function getAnneeDoctorant() {
        return $this->anneeDoctorant;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Doctorant
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->coursDoctorant = new \Doctrine\Common\Collections\ArrayCollection();
        $this->anneeDoctorant = 1;
        $this->statutDoctorant = "doctorant.statutDoctorant.actif";
    }

    /**
     * Add coursDoctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant
     * @return Doctorant
     */
    public function addCoursDoctorant(\Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant) {
        $this->coursDoctorant[] = $coursDoctorant;

        return $this;
    }

    /**
     * Remove coursDoctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant
     */
    public function removeCoursDoctorant(\Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant) {
        $this->coursDoctorant->removeElement($coursDoctorant);
    }

    /**
     * Get coursDoctorant
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCoursDoctorant() {
        return $this->coursDoctorant;
    }


    /**
     * Add coursPif
     *
     * @param \Edspim\Bundle\AppBundle\Entity\CoursPif $coursPif
     * @return Doctorant
     */
    public function addCoursPif(\Edspim\Bundle\AppBundle\Entity\CoursPif $coursPif)
    {
        $this->coursPif[] = $coursPif;

        return $this;
    }

    /**
     * Remove coursPif
     *
     * @param \Edspim\Bundle\AppBundle\Entity\CoursPif $coursPif
     */
    public function removeCoursPif(\Edspim\Bundle\AppBundle\Entity\CoursPif $coursPif)
    {
        $this->coursPif->removeElement($coursPif);
    }

    /**
     * Get coursPif
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCoursPif()
    {
        return $this->coursPif;
    }

   
}
