<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Enseignant
 *
 * @ORM\Table("Enseignant")
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\EnseignantRepository")
 */
class Enseignant {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $prenom;

    /**
     * @var email
     *
     * @ORM\Column(name="email", type="string", length=75)
     * 
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="labDpt", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $labDpt;

    /**
     * @var string
     *
     * @ORM\Column(name="institut", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $institut;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $fonction;

    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\EnseignantCours", mappedBy="enseignant", cascade={"persist"})
     */
    private $enseignantCours;

    /**
     * Fonction standard toString
     * 
     * @return string nom et prénom
     */
    public function __toString() {
        return $this->prenom . " " . $this->nom;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Enseignant
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Enseignant
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set labDpt
     *
     * @param string $labDpt
     * @return Enseignant
     */
    public function setLabDpt($labDpt) {
        $this->labDpt = $labDpt;

        return $this;
    }

    /**
     * Get labDpt
     *
     * @return string 
     */
    public function getLabDpt() {
        return $this->labDpt;
    }

    /**
     * Set institut
     *
     * @param string $institut
     * @return Enseignant
     */
    public function setInstitut($institut) {
        $this->institut = $institut;

        return $this;
    }

    /**
     * Get institut
     *
     * @return string 
     */
    public function getInstitut() {
        return $this->institut;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     * @return Enseignant
     */
    public function setFonction($fonction) {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string 
     */
    public function getFonction() {
        return $this->fonction;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Enseignant
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enseignantCours = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add enseignantCours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours
     * @return Enseignant
     */
    public function addEnseignantCour(\Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours)
    {
        $this->enseignantCours[] = $enseignantCours;

        return $this;
    }

    /**
     * Remove enseignantCours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours
     */
    public function removeEnseignantCour(\Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours)
    {
        $this->enseignantCours->removeElement($enseignantCours);
    }

    /**
     * Get enseignantCours
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEnseignantCours()
    {
        return $this->enseignantCours;
    }
    
    public function getCoursActif() {
        $actif = new \Doctrine\Common\Collections\ArrayCollection();
        foreach($this->enseignantCours as $ensCours) {
            if(in_array($ensCours->getCours()->getStatutCours(), array(
                "cours.statutCours.archive", 
                "cours.statutCours.inscription", 
                "cours.statutCours.ouvert"))) {
                $actif->add($ensCours);
            }
        }
        return $actif;
    }
}
