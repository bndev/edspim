<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SeanceDoctorant
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\SeanceDoctorantRepository")
 */
class SeanceDoctorant
{

    /**
     * @var boolean
     *
     * @ORM\Column(name="estPresent", type="boolean")
     */
    private $estPresent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePresence", type="datetime")
     */
    private $datePresence;
    
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Seance", inversedBy="seanceDoctorant")
     */
    private $seance;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Doctorant", inversedBy="seanceDoctorant")
     */
    private $doctorant;

    
    public function __construct() {
        $this->datePresence = new \DateTime();
        $this->estPresent = false;
    }
    
    
    public function __toString() {
        return $this->doctorant.'';
    }

    /**
     * Set estPresent
     *
     * @param boolean $estPresent
     * @return SeanceDoctorant
     */
    public function setEstPresent($estPresent)
    {
        $this->estPresent = $estPresent;

        return $this;
    }

    /**
     * Get estPresent
     *
     * @return boolean 
     */
    public function getEstPresent()
    {
        return $this->estPresent;
    }

    /**
     * Set datePresence
     *
     * @param \DateTime $datePresence
     * @return SeanceDoctorant
     */
    public function setDatePresence($datePresence)
    {
        $this->datePresence = $datePresence;

        return $this;
    }

    /**
     * Get datePresence
     *
     * @return \DateTime 
     */
    public function getDatePresence()
    {
        return $this->datePresence;
    }

    /**
     * Set seance
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Seance $seance
     * @return SeanceDoctorant
     */
    public function setSeance(\Edspim\Bundle\AppBundle\Entity\Seance $seance)
    {
        $this->seance = $seance;
        $seance->addSeanceDoctorant($this);
        return $this;
    }

    /**
     * Get seance
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Seance 
     */
    public function getSeance()
    {
        return $this->seance;
    }

    /**
     * Set doctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return SeanceDoctorant
     */
    public function setDoctorant(\Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant)
    {
        $this->doctorant = $doctorant;
        $doctorant->addSeanceDoctorant($this);
        return $this;
    }

    /**
     * Get doctorant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Doctorant 
     */
    public function getDoctorant()
    {
        return $this->doctorant;
    }
}
