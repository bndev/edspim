<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Seance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\SeanceRepository")
 */
class Seance {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSeance", type="datetime")
     */
    private $dateSeance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heureSeance", type="time")
     */
    private $heureSeance;

    /**
     * @var string
     *
     * @ORM\Column(name="typeSeance", type="string", length=50)
     * 
     * @Assert\Choice(choices = 
     *      {   "seance.typeSeance.cours", 
     *          "seance.typeSeance.td", 
     *          "seance.typeSeance.tp",
     *      }, 
     *      message = "Choisissez un statut valide."
     * )
     */
    private $typeSeance;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbHeures", type="integer")
     * 
     * @Assert\Range(
     *      min = 1,
     *      max = 50
     * )
     */
    private $nbHeures;

    /**
     * @var string
     *
     * @ORM\Column(name="salle", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $salle;

    /**
     * @var string
     *
     * @ORM\Column(name="batiment", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $batiment;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $lieu;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="session", type="integer")
     */
    private $session;

    /**
     * @var boolean
     *
     * @ORM\Column(name="presenceEnseignant", type="boolean")
     */
    private $presenceEnseignant;

    /**
     * @var boolean
     *
     * @ORM\Column(name="presenceED", type="boolean")
     */
    private $presenceED;

    /**
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Cours", inversedBy="seances")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cours;
    
    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\SeanceDoctorant", mappedBy="seance")
     */
    private $seanceDoctorant;
    
    public function __construct() {
        $this->dateSeance = new \DateTime();
        $this->presenceED =false;
        $this->presenceEnseignant = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set dateSeance
     *
     * @param \DateTime $dateSeance
     * @return Seance
     */
    public function setDateSeance($dateSeance) {
        $this->dateSeance = $dateSeance;

        return $this;
    }

    /**
     * Get dateSeance
     *
     * @return \DateTime 
     */
    public function getDateSeance() {
        return $this->dateSeance;
    }

    /**
     * Set heureSeance
     *
     * @param \DateTime $heureSeance
     * @return Seance
     */
    public function setHeureSeance($heureSeance) {
        $this->heureSeance = $heureSeance;

        return $this;
    }

    /**
     * Get heureSeance
     *
     * @return \DateTime 
     */
    public function getHeureSeance() {
        return $this->heureSeance;
    }

    /**
     * Set typeSeance
     *
     * @param string $typeSeance
     * @return Seance
     */
    public function setTypeSeance($typeSeance) {
        $this->typeSeance = $typeSeance;

        return $this;
    }

    /**
     * Get typeSeance
     *
     * @return string 
     */
    public function getTypeSeance() {
        return $this->typeSeance;
    }

    /**
     * Set nbHeures
     *
     * @param integer $nbHeures
     * @return Seance
     */
    public function setNbHeures($nbHeures) {
        $this->nbHeures = $nbHeures;

        return $this;
    }

    /**
     * Get nbHeures
     *
     * @return integer 
     */
    public function getNbHeures() {
        return $this->nbHeures;
    }

    /**
     * Set salle
     *
     * @param string $salle
     * @return Seance
     */
    public function setSalle($salle) {
        $this->salle = $salle;

        return $this;
    }

    /**
     * Get salle
     *
     * @return string 
     */
    public function getSalle() {
        return $this->salle;
    }

    /**
     * Set batiment
     *
     * @param string $batiment
     * @return Seance
     */
    public function setBatiment($batiment) {
        $this->batiment = $batiment;

        return $this;
    }

    /**
     * Get batiment
     *
     * @return string 
     */
    public function getBatiment() {
        return $this->batiment;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Seance
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Seance
     */
    public function setLieu($lieu) {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu() {
        return $this->lieu;
    }

    /**
     * Set cours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @return Seance
     */
    public function setCours(\Edspim\Bundle\AppBundle\Entity\Cours $cours) {
        $this->cours = $cours;

        return $this;
    }

    /**
     * Get cours
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Cours 
     */
    public function getCours() {
        return $this->cours;
    }


    /**
     * Set presenceEnseignant
     *
     * @param boolean $presenceEnseignant
     * @return Seance
     */
    public function setPresenceEnseignant($presenceEnseignant)
    {
        $this->presenceEnseignant = $presenceEnseignant;

        return $this;
    }

    /**
     * Get presenceEnseignant
     *
     * @return boolean 
     */
    public function getPresenceEnseignant()
    {
        return $this->presenceEnseignant;
    }

    /**
     * Set presenceED
     *
     * @param boolean $presenceED
     * @return Seance
     */
    public function setPresenceED($presenceED)
    {
        $this->presenceED = $presenceED;

        return $this;
    }

    /**
     * Get presenceED
     *
     * @return boolean 
     */
    public function getPresenceED()
    {
        return $this->presenceED;
    }

    /**
     * Set session
     *
     * @param integer $session
     * @return Seance
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return integer 
     */
    public function getSession()
    {
        return $this->session;
    }


    /**
     * Add seanceDoctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\SeanceDoctorant $seanceDoctorant
     * @return Seance
     */
    public function addSeanceDoctorant(\Edspim\Bundle\AppBundle\Entity\SeanceDoctorant $seanceDoctorant)
    {
        $this->seanceDoctorant[] = $seanceDoctorant;

        return $this;
    }

    /**
     * Remove seanceDoctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\SeanceDoctorant $seanceDoctorant
     */
    public function removeSeanceDoctorant(\Edspim\Bundle\AppBundle\Entity\SeanceDoctorant $seanceDoctorant)
    {
        $this->seanceDoctorant->removeElement($seanceDoctorant);
    }

    /**
     * Get seanceDoctorant
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeanceDoctorant()
    {
        return $this->seanceDoctorant;
    }
}
