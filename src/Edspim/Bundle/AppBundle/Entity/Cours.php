<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cours
 *
 * @ORM\Table("Cours")
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\CoursRepository")
 */
class Cours {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "1",
     *      max = "50"
     * )
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="acronyme", type="string", length=10)
     * 
     * @Assert\Length(
     *      min = "1",
     *      max = "10"
     * )
     */
    private $acronyme;

    /**
     * @var string
     *
     * @ORM\Column(name="typeCours", type="string", length=50)
     * 
     * @Assert\Choice(choices = 
     *      {   "cours.typeCours.formation", 
     *          "cours.typeCours.boite", 
     *          "cours.typeCours.coursED",
     *          "cours.typeCours.tutoCongres",
     *          "cours.typeCours.ecoleEte", 
     *          "cours.typeCours.autre", 
     *      }, 
     *      message = "Choisissez un type valide."
     * )
     */
    private $typeCours;

    /**
     * @var boolean
     *
     * @ORM\Column(name="oral", type="boolean")
     */
    private $oral;

    /**
     * @var boolean
     *
     * @ORM\Column(name="presentationOral", type="boolean")
     */
    private $presentationOral;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ecrit", type="boolean")
     */
    private $ecrit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ecritOral", type="boolean")
     */
    private $ecritOral;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rapportProjet", type="boolean")
     */
    private $rapportProjet;

    /**
     * @var boolean
     *
     * @ORM\Column(name="finalEcrit", type="boolean")
     */
    private $finalEcrit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiple", type="boolean")
     */
    private $multiple;

    /**
     * @var boolean
     *
     * @ORM\Column(name="autre", type="boolean")
     */
    private $autre;

    /**
     * @var string
     *
     * @ORM\Column(name="anneeFormation", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $anneeFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="anneeDebut", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $anneeDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="anneeFin", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $anneeFin;

    /**
     * @var string
     *
     * @ORM\Column(name="frequence", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $frequence;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxNbParticipants", type="integer")
     * 
     * @Assert\Range(
     *      min = 1,
     *      max = 1000
     * )
     */
    private $maxNbParticipants;

    /**
     * @var string
     *
     * @ORM\Column(name="salleReservee", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $salleReservee;

    /**
     * @var string
     *
     * @ORM\Column(name="batiment", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $batiment;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "1",
     *      max = "50"
     * )
     */
    private $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="acquis", type="string", length=1000, nullable=true)
     * 
     * @Assert\Length(
     *      max = "1000"
     * )
     */
    private $acquis;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=1000, nullable=true)
     * 
     * @Assert\Length(
     *      max = "1000"
     * )
     */
    private $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaireED", type="string", length=1000, nullable=true)
     * 
     * @Assert\Length(
     *      max = "1000"
     * )
     */
    private $commentaireED;

    /**
     * @var string
     *
     * @ORM\Column(name="connaissancesDemandees", type="string", length=1000, nullable=true)
     * 
     * @Assert\Length(
     *      max = "1000"
     * )
     */
    private $connaissancesDemandees;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModification", type="datetime")
     */
    private $dateModification;

    /**
     * @var integer
     *
     * @ORM\Column(name="session", type="integer")
     */
    private $session;

    /**
     * @var string
     *
     * @ORM\Column(name="statutCours", type="string", length=50)
     * 
     * @Assert\Choice(choices = 
     *      {   "cours.statutCours.brouillon", 
     *          "cours.statutCours.archive", 
     *          "cours.statutCours.inscription",
     *          "cours.statutCours.ouvert",
     *          "cours.statutCours.cache", 
     *      }, 
     *      message = "Choisissez un statut valide."
     * )
     */
    private $statutCours;

    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\Seance", mappedBy="cours", cascade={"persist", "remove"})
     */
    private $seances;

    /**
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createur;

    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\CoursDoctorant", mappedBy="cours")
     */
    private $coursDoctorant;

    /**
     * @ORM\OneToMany(targetEntity="Edspim\Bundle\AppBundle\Entity\EnseignantCours", mappedBy="cours", cascade={"persist"})
     */
    private $enseignantCours;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->seances = new \Doctrine\Common\Collections\ArrayCollection();
        $this->session = 0;
        $this->dateModification = new \DateTime();
        $this->maxNbParticipants = 1;
        $this->oral = false;
        $this->ecrit = false;
        $this->ecritOral = false;
        $this->multiple = false;
        $this->autre = false;
        $this->rapportProjet = false;
        $this->presentationOral = false;
        $this->finalEcrit = false;
    }
    

    /**
     * Fonction toString standard
     * 
     * @return type
     */
    public function __toString() {
        return $this->acronyme . " - " . $this->titre;
    }

    public function getSeancesActives() {
        $seancesActives = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->seances as $seance) {
            if ($seance->getSession() === $this->session) {
                $seancesActives->add($seance);
            }
        }
        return $seancesActives;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Cours
     */
    public function setTitre($titre) {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre() {
        return $this->titre;
    }

    /**
     * Set acronyme
     *
     * @param string $acronyme
     * @return Cours
     */
    public function setAcronyme($acronyme) {
        $this->acronyme = $acronyme;

        return $this;
    }

    /**
     * Get acronyme
     *
     * @return string 
     */
    public function getAcronyme() {
        return $this->acronyme;
    }

    /**
     * Set typeCours
     *
     * @param string $typeCours
     * @return Cours
     */
    public function setTypeCours($typeCours) {
        $this->typeCours = $typeCours;

        return $this;
    }

    /**
     * Get typeCours
     *
     * @return string 
     */
    public function getTypeCours() {
        return $this->typeCours;
    }

    /**
     * Set oral
     *
     * @param boolean $oral
     * @return Cours
     */
    public function setOral($oral) {
        $this->oral = $oral;

        return $this;
    }

    /**
     * Get oral
     *
     * @return boolean 
     */
    public function getOral() {
        return $this->oral;
    }

    /**
     * Set presentationOral
     *
     * @param boolean $presentationOral
     * @return Cours
     */
    public function setPresentationOral($presentationOral) {
        $this->presentationOral = $presentationOral;

        return $this;
    }

    /**
     * Get presentationOral
     *
     * @return boolean 
     */
    public function getPresentationOral() {
        return $this->presentationOral;
    }

    /**
     * Set ecrit
     *
     * @param boolean $ecrit
     * @return Cours
     */
    public function setEcrit($ecrit) {
        $this->ecrit = $ecrit;

        return $this;
    }

    /**
     * Get ecrit
     *
     * @return boolean 
     */
    public function getEcrit() {
        return $this->ecrit;
    }

    /**
     * Set ecritOral
     *
     * @param boolean $ecritOral
     * @return Cours
     */
    public function setEcritOral($ecritOral) {
        $this->ecritOral = $ecritOral;

        return $this;
    }

    /**
     * Get ecritOral
     *
     * @return boolean 
     */
    public function getEcritOral() {
        return $this->ecritOral;
    }

    /**
     * Set rapportProjet
     *
     * @param boolean $rapportProjet
     * @return Cours
     */
    public function setRapportProjet($rapportProjet) {
        $this->rapportProjet = $rapportProjet;

        return $this;
    }

    /**
     * Get rapportProjet
     *
     * @return boolean 
     */
    public function getRapportProjet() {
        return $this->rapportProjet;
    }

    /**
     * Set finalEcrit
     *
     * @param boolean $finalEcrit
     * @return Cours
     */
    public function setFinalEcrit($finalEcrit) {
        $this->finalEcrit = $finalEcrit;

        return $this;
    }

    /**
     * Get finalEcrit
     *
     * @return boolean 
     */
    public function getFinalEcrit() {
        return $this->finalEcrit;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple
     * @return Cours
     */
    public function setMultiple($multiple) {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean 
     */
    public function getMultiple() {
        return $this->multiple;
    }

    /**
     * Set autre
     *
     * @param boolean $autre
     * @return Cours
     */
    public function setAutre($autre) {
        $this->autre = $autre;

        return $this;
    }

    /**
     * Get autre
     *
     * @return boolean 
     */
    public function getAutre() {
        return $this->autre;
    }

    /**
     * Set anneeFormation
     *
     * @param string $anneeFormation
     * @return Cours
     */
    public function setAnneeFormation($anneeFormation) {
        $this->anneeFormation = $anneeFormation;

        return $this;
    }

    /**
     * Get anneeFormation
     *
     * @return string 
     */
    public function getAnneeFormation() {
        return $this->anneeFormation;
    }

    /**
     * Set anneeDebut
     *
     * @param string $anneeDebut
     * @return Cours
     */
    public function setAnneeDebut($anneeDebut) {
        $this->anneeDebut = $anneeDebut;

        return $this;
    }

    /**
     * Get anneeDebut
     *
     * @return string 
     */
    public function getAnneeDebut() {
        return $this->anneeDebut;
    }

    /**
     * Set anneeFin
     *
     * @param string $anneeFin
     * @return Cours
     */
    public function setAnneeFin($anneeFin) {
        $this->anneeFin = $anneeFin;

        return $this;
    }

    /**
     * Get anneeFin
     *
     * @return string 
     */
    public function getAnneeFin() {
        return $this->anneeFin;
    }

    /**
     * Set frequence
     *
     * @param string $frequence
     * @return Cours
     */
    public function setFrequence($frequence) {
        $this->frequence = $frequence;

        return $this;
    }

    /**
     * Get frequence
     *
     * @return string 
     */
    public function getFrequence() {
        return $this->frequence;
    }

    /**
     * Set maxNbParticipants
     *
     * @param integer $maxNbParticipants
     * @return Cours
     */
    public function setMaxNbParticipants($maxNbParticipants) {
        $this->maxNbParticipants = $maxNbParticipants;

        return $this;
    }

    /**
     * Get maxNbParticipants
     *
     * @return integer 
     */
    public function getMaxNbParticipants() {
        return $this->maxNbParticipants;
    }

    /**
     * Set salleReservee
     *
     * @param string $salleReservee
     * @return Cours
     */
    public function setSalleReservee($salleReservee) {
        $this->salleReservee = $salleReservee;

        return $this;
    }

    /**
     * Get salleReservee
     *
     * @return string 
     */
    public function getSalleReservee() {
        return $this->salleReservee;
    }

    /**
     * Set batiment
     *
     * @param string $batiment
     * @return Cours
     */
    public function setBatiment($batiment) {
        $this->batiment = $batiment;

        return $this;
    }

    /**
     * Get batiment
     *
     * @return string 
     */
    public function getBatiment() {
        return $this->batiment;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Cours
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Cours
     */
    public function setLieu($lieu) {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu() {
        return $this->lieu;
    }

    /**
     * Set acquis
     *
     * @param string $acquis
     * @return Cours
     */
    public function setAcquis($acquis) {
        $this->acquis = $acquis;

        return $this;
    }

    /**
     * Get acquis
     *
     * @return string 
     */
    public function getAcquis() {
        return $this->acquis;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Cours
     */
    public function setContenu($contenu) {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu() {
        return $this->contenu;
    }

    /**
     * Set commentaireED
     *
     * @param string $commentaireED
     * @return Cours
     */
    public function setCommentaireED($commentaireED) {
        $this->commentaireED = $commentaireED;

        return $this;
    }

    /**
     * Get commentaireED
     *
     * @return string 
     */
    public function getCommentaireED() {
        return $this->commentaireED;
    }

    /**
     * Set connaissancesDemandees
     *
     * @param string $connaissancesDemandees
     * @return Cours
     */
    public function setConnaissancesDemandees($connaissancesDemandees) {
        $this->connaissancesDemandees = $connaissancesDemandees;

        return $this;
    }

    /**
     * Get connaissancesDemandees
     *
     * @return string 
     */
    public function getConnaissancesDemandees() {
        return $this->connaissancesDemandees;
    }

    /**
     * Set dateModification
     *
     * @param \DateTime $dateModification
     * @return Cours
     */
    public function setDateModification($dateModification) {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return \DateTime 
     */
    public function getDateModification() {
        return $this->dateModification;
    }

    /**
     * Set session
     *
     * @param integer $session
     * @return Cours
     */
    public function setSession($session) {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return integer 
     */
    public function getSession() {
        return $this->session;
    }

    /**
     * Set statutCours
     *
     * @param string $statutCours
     * @return Cours
     */
    public function setStatutCours($statutCours) {
        $this->statutCours = $statutCours;

        return $this;
    }

    /**
     * Get statutCours
     *
     * @return string 
     */
    public function getStatutCours() {
        return $this->statutCours;
    }


    /**
     * Add seances
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Seance $seances
     * @return Cours
     */
    public function addSeance(\Edspim\Bundle\AppBundle\Entity\Seance $seances) {
        $this->seances[] = $seances;
        $seances->setCours($this);
        $seances->setSession($this->session);

        return $this;
    }

    /**
     * Remove seances
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Seance $seances
     */
    public function removeSeance(\Edspim\Bundle\AppBundle\Entity\Seance $seances) {
        $this->seances->removeElement($seances);
    }

    /**
     * Get seances
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeances() {
        return $this->seances;
    }

    /**
     * Set createur
     *
     * @param \Edspim\Bundle\UserBundle\Entity\User $createur
     * @return Cours
     */
    public function setCreateur(\Edspim\Bundle\UserBundle\Entity\User $createur) {
        $this->createur = $createur;

        return $this;
    }

    /**
     * Get createur
     *
     * @return \Edspim\Bundle\UserBundle\Entity\User 
     */
    public function getCreateur() {
        return $this->createur;
    }

    /**
     * Add coursDoctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant
     * @return Cours
     */
    public function addCoursDoctorant(\Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant) {
        $this->coursDoctorant[] = $coursDoctorant;

        return $this;
    }

    /**
     * Remove coursDoctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant
     */
    public function removeCoursDoctorant(\Edspim\Bundle\AppBundle\Entity\CoursDoctorant $coursDoctorant) {
        $this->coursDoctorant->removeElement($coursDoctorant);
    }

    /**
     * Get coursDoctorant
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCoursDoctorant() {
        return $this->coursDoctorant;
    }

    public function getCoursDoctorantInscription() {
        $coursInscription = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->coursDoctorant as $cours) {
            if ("coursdoc.statut.inscription" === $cours->getStatut()) {
                $coursInscription->add($cours);
            }
        }
        return $coursInscription;
    }

    public function getCoursDoctorantOuvert() {
        $coursInscription = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->coursDoctorant as $cours) {
            if ("coursdoc.statut.actif" === $cours->getStatut()) {
                $coursInscription->add($cours);
            }
        }
        return $coursInscription;
    }

    /**
     * Add enseignantCours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours
     * @return Cours
     */
    public function addEnseignantCour(\Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours)
    {
        $this->enseignantCours[] = $enseignantCours;
        $enseignantCours->setCours($this);
        return $this;
    }

    /**
     * Remove enseignantCours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours
     */
    public function removeEnseignantCour(\Edspim\Bundle\AppBundle\Entity\EnseignantCours $enseignantCours)
    {
        $this->enseignantCours->removeElement($enseignantCours);
    }

    /**
     * Get enseignantCours
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEnseignantCours()
    {
        return $this->enseignantCours;
    }
    
}
