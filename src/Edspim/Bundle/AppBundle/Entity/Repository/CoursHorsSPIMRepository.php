<?php

namespace Edspim\Bundle\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CoursHorsSPIMRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CoursHorsSPIMRepository extends EntityRepository
{
    
    public function findNbCoursHS() {
        
        $qb = $this->createQueryBuilder('e')
                ->select('COUNT(e)');
        $qb->where($qb->expr()->eq('e.statut', "'demande.statut.attente'"));
        return $qb->getQuery()->getSingleScalarResult();
    }
}
