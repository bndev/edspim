<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CoursHorsSPIM
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\CoursHorsSPIMRepository")
 */
class CoursHorsSPIM {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=50)
     * 
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "50 caractères max"
     * )
     */
    private $intitule;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=50)
     * 
     * @Assert\Length(
     *      max = "50",
     *      maxMessage = "30 caractères max"
     * )
     */
    private $lieu;

    /**
     * @var integer
     *
     * @ORM\Column(name="annee", type="integer")
     * 
     * @Assert\Range(
     *      min = 1,
     *      max = 3,
     *      minMessage = "Année 1 minimum",
     *      maxMessage = "Année 3 maximum"
     * )
     */
    private $annee;

    /**
     * @var string
     *
     * @ORM\Column(name="typeFormation", type="string", length=50)
     * 
     * @Assert\Choice(choices = 
     *      {   "cours.typeCours.formation", 
     *          "cours.typeCours.boite", 
     *          "cours.typeCours.coursED",
     *          "cours.typeCours.tutoCongres",
     *          "cours.typeCours.ecoleEte", 
     *          "cours.typeCours.autre", 
     *      }, 
     *      message = "Choisissez un type valide."
     * )
     */
    private $typeFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="categorieSouhaitee", type="string", length=50)
     * 
     * @Assert\Choice(choices = 
     *      {   "cours.typeCours.formation", 
     *          "cours.typeCours.boite", 
     *          "cours.typeCours.sciences",
     *      }, 
     *      message = "Choisissez un type valide."
     * )
     */
    private $categorieSouhaitee;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbHeures", type="integer")
     * 
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      minMessage = "0 minimum",
     *      maxMessage = "Trop d'heure au compteur"
     * )
     */
    private $nbHeures;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbJours", type="integer")
     * 
     * @Assert\Range(
     *      min = 0,
     *      max = 150,
     *      minMessage = "0 minimum",
     *      maxMessage = "Trop de jours au compteur"
     * )
     */
    private $nbJours;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=50)
     * 
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255, nullable=true)
     * 
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "1000 caractères max"
     * )
     */
    private $commentaire;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaireED", type="string", length=1000, nullable=true)
     * 
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "1000 caractères max"
     * )
     */
    private $commentaireED;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDemande", type="datetime")
     * 
     */
    private $dateDemande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateValidation", type="datetime", nullable=true)
     */
    private $dateValidation;

    /**
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Doctorant")
     */
    private $doctorant;

    /**
     * @var string
     *
     * @ORM\Column(name="credential", type="string", length=255)
     */
    private $credential;

    public function __construct() {
        $this->dateDemande = new \DateTime();
        $this->credential = uniqid("co2_", true);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     * @return CoursHorsSPIM
     */
    public function setIntitule($intitule) {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string 
     */
    public function getIntitule() {
        return $this->intitule;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return CoursHorsSPIM
     */
    public function setLieu($lieu) {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu() {
        return $this->lieu;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     * @return CoursHorsSPIM
     */
    public function setAnnee($annee) {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return integer 
     */
    public function getAnnee() {
        return $this->annee;
    }

    /**
     * Set typeFormation
     *
     * @param string $typeFormation
     * @return CoursHorsSPIM
     */
    public function setTypeFormation($typeFormation) {
        $this->typeFormation = $typeFormation;

        return $this;
    }

    /**
     * Get typeFormation
     *
     * @return string 
     */
    public function getTypeFormation() {
        return $this->typeFormation;
    }

    /**
     * Set categorieSouhaitee
     *
     * @param string $categorieSouhaitee
     * @return CoursHorsSPIM
     */
    public function setCategorieSouhaitee($categorieSouhaitee) {
        $this->categorieSouhaitee = $categorieSouhaitee;

        return $this;
    }

    /**
     * Get categorieSouhaitee
     *
     * @return string 
     */
    public function getCategorieSouhaitee() {
        return $this->categorieSouhaitee;
    }

    /**
     * Set nbHeures
     *
     * @param integer $nbHeures
     * @return CoursHorsSPIM
     */
    public function setNbHeures($nbHeures) {
        $this->nbHeures = $nbHeures;

        return $this;
    }

    /**
     * Get nbHeures
     *
     * @return integer 
     */
    public function getNbHeures() {
        return $this->nbHeures;
    }

    /**
     * Set statut
     *
     * @param string $statut
     * @return CoursHorsSPIM
     */
    public function setStatut($statut) {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string 
     */
    public function getStatut() {
        return $this->statut;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     * @return CoursHorsSPIM
     */
    public function setCommentaire($commentaire) {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire() {
        return $this->commentaire;
    }

    /**
     * Set dateDemande
     *
     * @param \DateTime $dateDemande
     * @return CoursHorsSPIM
     */
    public function setDateDemande($dateDemande) {
        $this->dateDemande = $dateDemande;

        return $this;
    }

    /**
     * Get dateDemande
     *
     * @return \DateTime 
     */
    public function getDateDemande() {
        return $this->dateDemande;
    }

    /**
     * Set dateValidation
     *
     * @param \DateTime $dateValidation
     * @return CoursHorsSPIM
     */
    public function setDateValidation($dateValidation) {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    /**
     * Get dateValidation
     *
     * @return \DateTime 
     */
    public function getDateValidation() {
        return $this->dateValidation;
    }

    /**
     * Set nbJours
     *
     * @param integer $nbJours
     * @return CoursHorsSPIM
     */
    public function setNbJours($nbJours) {
        $this->nbJours = $nbJours;

        return $this;
    }

    /**
     * Get nbJours
     *
     * @return integer 
     */
    public function getNbJours() {
        return $this->nbJours;
    }

    /**
     * Set commentaireED
     *
     * @param string $commentaireED
     * @return CoursHorsSPIM
     */
    public function setCommentaireED($commentaireED) {
        $this->commentaireED = $commentaireED;

        return $this;
    }

    /**
     * Get commentaireED
     *
     * @return string 
     */
    public function getCommentaireED() {
        return $this->commentaireED;
    }

    /**
     * Set doctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return CoursHorsSPIM
     */
    public function setDoctorant(\Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant = null) {
        $this->doctorant = $doctorant;

        return $this;
    }

    /**
     * Get doctorant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Doctorant 
     */
    public function getDoctorant() {
        return $this->doctorant;
    }


    /**
     * Set credential
     *
     * @param string $credential
     * @return CoursHorsSPIM
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;

        return $this;
    }

    /**
     * Get credential
     *
     * @return string 
     */
    public function getCredential()
    {
        return $this->credential;
    }
}
