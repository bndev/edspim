<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnseignantCours
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\EnseignantCoursRepository")
 */
class EnseignantCours
{

     /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Cours", inversedBy="enseignantCours")
     */
    private $cours;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Enseignant", inversedBy="enseignantCours")
     */
    private $enseignant;

    /**
     * @var integer
     *
     * @ORM\Column(name="heureCours", type="integer")
     */
    private $heureCours;

    /**
     * @var integer
     *
     * @ORM\Column(name="heureTD", type="integer")
     */
    private $heureTD;

    /**
     * @var integer
     *
     * @ORM\Column(name="heureTP", type="integer")
     */
    private $heureTP;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estResponsable", type="boolean")
     */
    private $estResponsable;


    /**
     * Constructor
     */
    public function __construct() {
        $this->heureCours = 0;
        $this->heureTD = 0;
        $this->heureTP = 0;
        $this->estResponsable = false;
    }
    
    /**
     * Set heureCours
     *
     * @param integer $heureCours
     * @return EnseignantCours
     */
    public function setHeureCours($heureCours)
    {
        $this->heureCours = $heureCours;

        return $this;
    }

    /**
     * Get heureCours
     *
     * @return integer 
     */
    public function getHeureCours()
    {
        return $this->heureCours;
    }

    /**
     * Set heureTD
     *
     * @param integer $heureTD
     * @return EnseignantCours
     */
    public function setHeureTD($heureTD)
    {
        $this->heureTD = $heureTD;

        return $this;
    }

    /**
     * Get heureTD
     *
     * @return integer 
     */
    public function getHeureTD()
    {
        return $this->heureTD;
    }

    /**
     * Set heureTP
     *
     * @param integer $heureTP
     * @return EnseignantCours
     */
    public function setHeureTP($heureTP)
    {
        $this->heureTP = $heureTP;

        return $this;
    }

    /**
     * Get heureTP
     *
     * @return integer 
     */
    public function getHeureTP()
    {
        return $this->heureTP;
    }

    /**
     * Set estResponsable
     *
     * @param boolean $estResponsable
     * @return EnseignantCours
     */
    public function setEstResponsable($estResponsable)
    {
        $this->estResponsable = $estResponsable;

        return $this;
    }

    /**
     * Get estResponsable
     *
     * @return boolean 
     */
    public function getEstResponsable()
    {
        return $this->estResponsable;
    }

    /**
     * Set cours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @return EnseignantCours
     */
    public function setCours(\Edspim\Bundle\AppBundle\Entity\Cours $cours)
    {
        $this->cours = $cours;
        //$cours->addEnseignantCour($this);
        return $this;
    }

    /**
     * Get cours
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Cours 
     */
    public function getCours()
    {
        return $this->cours;
    }

    /**
     * Set enseignant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Enseignant $enseignant
     * @return EnseignantCours
     */
    public function setEnseignant(\Edspim\Bundle\AppBundle\Entity\Enseignant $enseignant)
    {
        $this->enseignant = $enseignant;
        $enseignant->addEnseignantCour($this);
        return $this;
    }

    /**
     * Get enseignant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Enseignant 
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }
}
