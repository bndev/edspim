<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Table("Commentaire")
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=2000)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCommentaire", type="datetime")
     */
    private $dateCommentaire;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;
    
    /**
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Doctorant")
     * @ORM\JoinColumn(nullable=false)
     */
    private $doctorant;
    
    /**
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Cours")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cours;
    
    
    public function __construct() {
        $this->dateCommentaire = new \DateTime();
        $this->visible = false;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Commentaire
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set dateCommentaire
     *
     * @param \DateTime $dateCommentaire
     * @return Commentaire
     */
    public function setDateCommentaire($dateCommentaire)
    {
        $this->dateCommentaire = $dateCommentaire;

        return $this;
    }

    /**
     * Get dateCommentaire
     *
     * @return \DateTime 
     */
    public function getDateCommentaire()
    {
        return $this->dateCommentaire;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Commentaire
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set doctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return Commentaire
     */
    public function setDoctorant(\Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant)
    {
        $this->doctorant = $doctorant;

        return $this;
    }

    /**
     * Get doctorant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Doctorant 
     */
    public function getDoctorant()
    {
        return $this->doctorant;
    }

    /**
     * Set cours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @return Commentaire
     */
    public function setCours(\Edspim\Bundle\AppBundle\Entity\Cours $cours)
    {
        $this->cours = $cours;
        return $this;
    }

    /**
     * Get cours
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Cours 
     */
    public function getCours()
    {
        return $this->cours;
    }
}
