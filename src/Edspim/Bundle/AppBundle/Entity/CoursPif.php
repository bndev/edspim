<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CoursPif
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CoursPif
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomCoursPif", type="string", length=60)
     * 
     * @Assert\Length(
     *      min = "1",
     *      max = "50",
     *      minMessage = "1 caractères mini",
     *      maxMessage = "50 caractères max"
     * )
     */
    private $nomCoursPif;

    /**
     * @var integer
     *
     * @ORM\Column(name="anneePif", type="integer")
     * 
     * @Assert\Range(
     *      min = 1,
     *      max = 3,
     *      minMessage = "Annee 1 minimum",
     *      maxMessage = "Annee 3 maximum"
     * )
     */
    private $anneePif;

    /**
     * @var string
     *
     * @ORM\Column(name="typeCours", type="string", length=50)
     * 
     * @Assert\Choice(choices = 
     *      {   "cours.typeCours.formation", 
     *          "cours.typeCours.boite", 
     *          "cours.typeCours.coursED",
     *          "cours.typeCours.tutoCongres",
     *          "cours.typeCours.ecoleEte", 
     *          "cours.typeCours.autre", 
     *      }, 
     *      message = "Choisissez un type valide."
     * )
     */
    private $typeCours;

    /**
     * @var integer
     *
     * @ORM\Column(name="heureValidees", type="integer")
     */
    private $heureValidees;

    /**
     * @var string
     *
     * @ORM\Column(name="credential", type="string", length=255)
     */
    private $credential;
    
    /**
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Doctorant", inversedBy="coursPif")
     * @ORM\JoinColumn(nullable=false)
     */
    private $doctorant;

    
    public function __construct() {
        $this->heureValidees = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomCoursPif
     *
     * @param string $nomCoursPif
     * @return CoursPif
     */
    public function setNomCoursPif($nomCoursPif)
    {
        $this->nomCoursPif = $nomCoursPif;

        return $this;
    }

    /**
     * Get nomCoursPif
     *
     * @return string 
     */
    public function getNomCoursPif()
    {
        return $this->nomCoursPif;
    }

    /**
     * Set anneePif
     *
     * @param integer $anneePif
     * @return CoursPif
     */
    public function setAnneePif($anneePif)
    {
        $this->anneePif = $anneePif;

        return $this;
    }

    /**
     * Get anneePif
     *
     * @return integer 
     */
    public function getAnneePif()
    {
        return $this->anneePif;
    }

    /**
     * Set typeCours
     *
     * @param string $typeCours
     * @return CoursPif
     */
    public function setTypeCours($typeCours)
    {
        $this->typeCours = $typeCours;

        return $this;
    }

    /**
     * Get typeCours
     *
     * @return string 
     */
    public function getTypeCours()
    {
        return $this->typeCours;
    }

    /**
     * Set heureValidees
     *
     * @param integer $heureValidees
     * @return CoursPif
     */
    public function setHeureValidees($heureValidees)
    {
        $this->heureValidees = $heureValidees;

        return $this;
    }

    /**
     * Get heureValidees
     *
     * @return integer 
     */
    public function getHeureValidees()
    {
        return $this->heureValidees;
    }

    /**
     * Set credential
     *
     * @param string $credential
     * @return CoursPif
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;

        return $this;
    }

    /**
     * Get credential
     *
     * @return string 
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * Set doctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return CoursPif
     */
    public function setDoctorant(\Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant)
    {
        $this->doctorant = $doctorant;
        $doctorant->addCoursPif($this);
        return $this;
    }

    /**
     * Get doctorant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Doctorant 
     */
    public function getDoctorant()
    {
        return $this->doctorant;
    }
}
