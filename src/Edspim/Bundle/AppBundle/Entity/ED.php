<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ED
 *
 * @ORM\Table("ED")
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\EDRepository")
 */
class ED {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=50)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $prenom;

    /**
     * @var email
     *
     * @ORM\Column(name="email", type="string", length=75)
     * 
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=50, nullable=true)
     * 
     * @Assert\Length(
     *      min = "2",
     *      max = "50"
     * )
     */
    private $fonction;

    /**
     * Fonction standard toString
     * 
     * @return string nom et prénom
     */
    public function __toString() {
        return $this->prenom . " " . $this->nom;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ED
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return ED
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     * @return ED
     */
    public function setFonction($fonction) {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string 
     */
    public function getFonction() {
        return $this->fonction;
    }


    /**
     * Set email
     *
     * @param string $email
     * @return ED
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
}
