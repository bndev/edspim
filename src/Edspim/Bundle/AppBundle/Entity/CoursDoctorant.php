<?php

namespace Edspim\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cours
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Edspim\Bundle\AppBundle\Entity\Repository\CoursDoctorantRepository")
 * 
 */
class CoursDoctorant {

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Cours", inversedBy="coursDoctorant")
     */
    private $cours;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Edspim\Bundle\AppBundle\Entity\Doctorant", inversedBy="coursDoctorant")
     */
    private $doctorant;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=50)
     */
    private $statut;
    
    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="modification", type="datetime")
     */
    private $modification;
    
    /**
     * @var string
     *
     * @ORM\Column(name="credential", type="string", length=255)
     */
    private $credential;


    public function __construct() {
        $this->statut = "coursdoc.statut.inscription";
        $this->modification = new \DateTime();
        $this->credential = uniqid("co1_", true);
    }
    
    public function __toString() {
        return $this->doctorant.'';
    }
    
    /**
     * Set statut
     *
     * @param string $statut
     * @return CoursDoctorant
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string 
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set modification
     *
     * @param \DateTime $modification
     * @return CoursDoctorant
     */
    public function setModification($modification)
    {
        $this->modification = $modification;

        return $this;
    }

    /**
     * Get modification
     *
     * @return \DateTime 
     */
    public function getModification()
    {
        return $this->modification;
    }

    /**
     * Set cours
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Cours $cours
     * @return CoursDoctorant
     */
    public function setCours(\Edspim\Bundle\AppBundle\Entity\Cours $cours)
    {
        $this->cours = $cours;
        $cours->addCoursDoctorant($this);

        return $this;
    }

    /**
     * Get cours
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Cours 
     */
    public function getCours()
    {
        return $this->cours;
    }
    

    /**
     * Set doctorant
     *
     * @param \Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant
     * @return CoursDoctorant
     */
    public function setDoctorant(\Edspim\Bundle\AppBundle\Entity\Doctorant $doctorant)
    {
        $this->doctorant = $doctorant;
        $doctorant->addCoursDoctorant($this);

        return $this;
    }

    /**
     * Get doctorant
     *
     * @return \Edspim\Bundle\AppBundle\Entity\Doctorant 
     */
    public function getDoctorant()
    {
        return $this->doctorant;
    }

    /**
     * Set credential
     *
     * @param string $credential
     * @return CoursDoctorant
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;

        return $this;
    }

    /**
     * Get credential
     *
     * @return string 
     */
    public function getCredential()
    {
        return $this->credential;
    }
}
